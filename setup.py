from setuptools import setup
import pip


# package required
install_packages = ["numpy", "matplotlib", "numpy_indexed"]

print("Installing bp project, the following packages are required:",
      install_packages)

# numpy needs to be installed using pypi for a precompiled version
try:
    import numpy
    print(numpy.version.version)
except ImportError:
    pip.main(["install", "numpy"])

# matplotlib to be installed using pypi for a precompiled version
try:
    import matplotlib
except ImportError:
    pip.main(["install", "matplotlib"])

setup(name="bpproject",
      url="https://gitlab.com/UdM/BPproject",  # access rights
      # on gitlab must be granted
      install_requires=install_packages,
      )
