import numpy as np
from pybinit.utils import WFKReader


class WFK:
    def __init__(self, path=None, data=None, matrix_form=True):
        """Takes a formatted wavefunction file, parses it and transform it
        to an usable array.

        Parameters
        ----------
        path : str, optional
               The path to a WFK file. If None, data is read directly.
        data : dict, optional
               The data of a WFK file. If None, data comes from the file
               given in path.
        matrix_form : bool, optional
                      If True, data will be organized in matrix format
                      for easier analysis.
        """
        # get data from file
        if path is not None and data is None:
            reader = WFKReader(path)
            data = reader.data
        elif path is None and data is not None:
            # ok do nothing take data directly from arg
            pass
        else:
            raise ValueError("Data and path cannot be both None.")
        (self._unique_x,
         self._unique_y,
         self._unique_z,
         self.unit_cell_volume) = self._get_unit_cell_volume(data)
        self.matrix_form = self._check_matrix_form(data)
        if matrix_form and not self.matrix_form:
            # if data not already in matrix format
            real, im, x, y, z = self.format_data(data)
            self.matrix_form = True
        elif matrix_form and matrix_form:
            # data already ordered in matrix form
            (real, im, x, y, z) = (data["real"], data["imaginary"],
                                   data["x"], data["y"], data["z"])
        else:
            self.matrix_form = False
            x = data["x"]
            y = data["y"]
            z = data["z"]
            real = data["real"]
            im = data["imaginary"]
        self.data = {'x': x, 'y': y, 'z': z, 'real': real, 'imaginary': im}

    def _check_matrix_form(self, data):
        # check the format of data (either matrix form or flat array)
        rshape = data["real"].shape
        ishape = data["imaginary"].shape
        if rshape != ishape:
            raise TypeError("Imaginary data have not the same format than"
                            "Real data!")
        elif len(rshape) > 1:
            # shape is already matrix
            # check consistency
            assert rshape[0] == len(data["x"])
            assert rshape[1] == len(data["y"])
            assert rshape[2] == len(data["z"])
            return True
        else:
            # shape is flat
            assert rshape[0] == len(data["x"])
            assert len(data["x"]) == len(data["y"])
            assert len(data["z"]) == len(data["y"])
            return False

    def format_data(self, data):
        """Format raw data from a WFK file.

        Parameters
        ----------
        data : dict
               It contains the x,y,z coordinates (in Angstroms!!)
               with the real and imaginary
               part of the WFK.
        """
        # Remove duplicates
        x = self._unique_x
        y = self._unique_y
        z = self._unique_z
        lx = len(x)
        ly = len(y)
        lz = len(z)
        size = (len(x), len(y), len(z))
        real = np.zeros(size)
        im = np.zeros(size)
        # order data in a 3D matrix instead of flat array
        for iz in range(lz):
            for iy in range(ly):
                for ix in range(lx):
                    i = ix + iy * lx + iz * lx * ly
                    real[ix, iy, iz] = data["real"][i]
                    im[ix, iy, iz] = data["imaginary"][i]
        # the method below is for an unordered file
        # for ix, xx in enumerate(x):
        #     where_x = np.where(data['x'] == xx)
        #     for iy, yy in enumerate(y):
        #         where_y = np.where(data['y'] == yy)
        #         for iz, zz in enumerate(z):
        #             where_z = np.where(data['z'] == zz)
        #             index = np.intersect1d(np.intersect1d(where_x, where_y),
        #                                    where_z)
        #             real[ix, iy, iz] = data['real'][index]
        #             im[ix, iy, iz] = data['imaginary'][index]
        return real, im, x, y, z

    def _get_unit_cell_volume(self, data):
        # get the unit cell volume from coordinates.
        # volume in A^3
        # get rid of duplicates and order coordinates
        x = np.unique(data["x"])
        y = np.unique(data["y"])
        z = np.unique(data["z"])
        dx, dy, dz = x[1], y[1], z[1]
        nx, ny, nz = len(x), len(y), len(z)
        return x, y, z, dx * nx * dy * ny * dz * nz

    def norm(self):
        """Computes the normalization of the WFK.

        Norm is just the integral of the square norm of the WFK.
        For a uniform FFT grid, it is just the sum of each point
        divided by the number of points in the grid.
        """
        return self.scalar_prod(self.dagger())

    def dagger(self):
        """Returns the complex conjugate of the WFK.

        Returns
        -------
        WFK object which is the dagger of the original one.
        """
        d = self.data.copy()
        d["imaginary"] = np.multiply(self.data["imaginary"], -1)
        return WFK(data=d, matrix_form=self.matrix_form)

    def __mul__(self, wfk2):
        # mutliplication of 2 complex function
        # (a + ib)(c + id) = ac - bd + i(ad + bc)
        a = self.data["real"]
        b = self.data["imaginary"]
        c = wfk2.data["real"]
        d = wfk2.data["imaginary"]
        return_real = np.subtract(np.multiply(a, c),
                                  np.multiply(b, d))
        return_im = np.add(np.multiply(a, d),
                           np.multiply(b, c))
        return return_real, return_im

    def scalar_prod(self, wfk2, operator=None):
        """Computes the scalar product of two WFK defined on the same grid.

        Computes integral of wfk2 * self which is sum of each element
        of wfk2 * self / Number of FFT grid points.

        Parameters
        ----------
        wfk2 : Another WFK object that contains data of another WFK file.
               Format must be the same between WFK.
        operator : function, optional
                   An operator that acts on the second WFK.
                   If None, no operator is applied (identity).
        Returns
        -------
        float1 : real value of scalar product
        float2 : imaginary value of scalar product
        """
        assert (self.data["x"] == wfk2.data["x"]).all()
        assert (self.data["y"] == wfk2.data["y"]).all()
        assert (self.data["z"] == wfk2.data["z"]).all()
        assert self.matrix_form == wfk2.matrix_form
        N = np.prod(np.shape(self.data["real"]))
        second_element = wfk2
        if operator is not None:
            second_element = operator(second_element)
        real, im = self * second_element
        return np.sum(real) / N, np.sum(im) / N
