from k_computer import K_computer, INTERACTIONS


class Intensity:
    def __init__(self, pp_or_pd):
        """Compute the intensity depending of the process.
        """
        Ks = self.get_Ks(pp_or_pd)
        self.I = self.compute_I(Ks, pp_or_pd)
        
    def compute_I(self, Ks, pp_or_pd):
        """Compute the intensity from the K elements.
        """
        sum_ = self.sum_Ks(Ks, pp_or_pd)
        return self.complex_sq_norm(sum_)
    
    def sum_Ks(self, Ks, pp_or_pd):
        """Sum the Ks.
        
        Depending of the process studied, the sum will differ.
        """
        return 1

    def complex_sq_norm(self, matrix):
        """Most efficient way to compute a squared complex norm of a matrix.
        """
        return 1
