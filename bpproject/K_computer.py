from .wfk import WFK


INTERACTIONS = ("ee1", "ee2", "hh1", "hh2", "eh1", "eh2", "he1", "he2")
TYPES = ("pd", "pp")


class K_computer:
    def __init__(self, pp_or_pd, interaction, states):
        """Computes the K element of a given interaction.

        pp_or_pd : str {"pd", "pp"}
                   Specifies a phonon-defect or phonon-phonon K element.
        interaction : str {"ee1", "eh2", ...}
                      Specifies the interaction type for the K element.
        states : list
                 The list of the four WFK relevent for this K.
        """
        if pp_or_pd not in TYPES:
            raise ValueError("%s not a valid interaction type." % pp_or_pd)
        if interaction not in INTERACTIONS:
            raise ValueError("%s not a valid interaction." % interaction)

        operators = self.get_operators(pp_or_pd, interaction)
        denominator = self.get_denominators(pp_or_pd, interaction)
        bras, kets = self._order_states(states, interaction)
        self.K = self.compute_K(operators, denominator, bras, kets)

    def scalar_product(self, bra, ket, operator):
        """Makes the braket integration with an operator between two states.
        """
        
    def compute_K(self, operators_list, denominator, states):
        """Compute one K element with the operators and denominators.
        """
        K = 1
        # order states
        for i, O in enumerate(operators_list):
            # call the operator with the corresponding args and kwargs (to
            # determine later)
            K *=  self.scalar_product(bras[i], kets[i], O)
        return K / denominator
    
    def _order_states(self, states, interaction):
        # returns the list of ordered states as listed in the article
        bras = []
        kets = []
        return bras, kets

    def get_denominator(self, pp_or_pd, interaction):
        """Get the denominators of a K element.
        """
        return 1

    def get_operators(self, pp_or_pd, interaction):
        """Return the Operators of the K elements.
        """
        # For now, only the identity operator is given.
        def identity(*args, **kwargs):
            return 1
        D_in = identity
        D_out = identity
        O1 = identity
        O2 = identity
        return (D_in, D_out, O1, O2)
