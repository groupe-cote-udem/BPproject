from .bandstructures import ElectronsBandstructure, PhononsBandstructure
from .resonance_saver import ResonanceSaver
from .files_getter import FilesGetter
from multiprocessing import Pool
from .routines import find_map_sorted, get_phonon_subres
from bpproject import ALL_INTERACTIONS
import numpy as np
import logging
import os


NPROC = os.cpu_count()


class ResonanceChecker:
    def __init__(self,
                 eL,
                 kgrid=None,
                 qgrid=None,
                 nlayer=1,
                 electron_bandstructure=None,
                 electron_kpts=None,
                 phonon_qpts=None,
                 phonon_bandstructure=None,
                 alpha=0.01,
                 interactions="all",
                 phonon_branches=(6, 10),
                 reduced_only=False,
                 compute_second_third=True,
                 slow_paral=False,
                 paral_nproc=NPROC,
                 save_resonance=None,
                 kplusq=None,
                 loglevel=logging.INFO):
        """Resonance Checker. It checks for each available transitions
        that can be made within the electron bandstructure in conjonction
        with the phonon bandstructure such that double resonance can be
        achieved.

        Parameters
        ----------
        eL : float
             Laser energy (in eV).
        kgrid : int, optional
                Size of kpt grid (100 for 100x100 grid). Cannot be none
                if electron_bandstructure and electron_kpts are none also.
        qgrid : int, optional
                Same as kgrid but for the qpt grid.
        nlayer : int, optional, {1 or 2}
                 Number of phosphorene layer to consider.
        electron_bandstructure : str, optional
                                 The path to the electron bandstructure.
        phonon_bandstructure : str, optional
                               The path to the phonons bandstructure.
        phonon_qpts : str, optional
                      The path to the phonons list of qpts.
        alpha : float
                tolerance parameter for the resonance condition (in eV)
        interactions : tuple, optional
                       Gives the interaction type. E.G. ee1, ee2, eh1,
                       to compute...
        compute_second_third : bool, optional
                               If False, the second and third resonances
                               won't be computed (only first will be).
        reduced_only : bool, optional
                       If True, the first resonance will be computed only
                       in the first quadrant of the BZ.
        phonon_branches : tuple, optional
                          Gives the index numbers of the phonon
                          branches to consider for resonance. (optical
                          phonons starts at 3).
        slow_paral : bool, optional
                     If True, the parallelization process will be slow down
                     to let computation finish before launching new ones.
        paral_nproc : int, optional
                      Number of cpus to use for parallelization scheme.
                      By default, this number is the maximal number of cores
                      on the computer.
        save_resonance : str, optional
                         If None, no loading nor saving resonances to a file.
                         If not None, it must be the path to a directory
                         containing the resonances files if you want to load
                         data (to prevent computation but still showing graphs)
                         If no files containing resonances is present, they
                         will be computed and then, they will be saved.
        kplusq : float, optional
                 If it is 1, the electrons/holes will not be allowed to diffuse
                 further than a box of sides given by the maximum and minimum
                 of first resonance points. This factor multiplies the size
                 of the box. If None, no global mask is used.
        loglevel : int, optional
                   Gives the level for the logger object.
        """
        logging.basicConfig()
        self._logger = logging.getLogger("Resonance")
        self._logger.setLevel(loglevel)

        self.eL = eL
        self.alpha = alpha
        self.paral_nproc = paral_nproc
        self._logger.info("Getting phonon data.")
        if electron_bandstructure is None and (electron_kpts is None and
                                               kgrid is None):
            raise ValueError("No electrons grids given.")
        elif electron_bandstructure is not None and (electron_kpts is not None
                                                     and kgrid is not None):
            raise ValueError("Which electrons grids to take?")
        if phonon_bandstructure is None and (phonon_qpts is None and
                                             qgrid is None):
            raise ValueError("No phonons grids given.")
        elif phonon_bandstructure is not None and (phonon_qpts is not None
                                                   and qgrid is not None):
            raise ValueError("Which phonons grids to take?")
        if qgrid is not None or kgrid is not None:
            getter = FilesGetter(kgrid or 100, qgrid or 100, nlayer)
            if qgrid is not None:
                phonon_qpts = getter.pqpts
                phonon_bandstructure = getter.ppath
            if kgrid is not None:
                electron_kpts = getter.ekpts
                electron_bandstructure = getter.epath
        self.phonons = PhononsBandstructure(phonon_bandstructure, phonon_qpts,
                                            flatten=True)

        self._logger.info("Phonon data gathered.")
        self._logger.info("Getting Electron data.")
        self.electrons = ElectronsBandstructure(electron_bandstructure,
                                                electron_kpts,
                                                flatten=True)
        scissor_shift = self.electrons.data["exp_gap"] - self.electrons.gap
        self._logger.info("Exp gap = %.2f, dft gap = %.2f => scissor = %.2f" %
                          (self.electrons.data["exp_gap"], self.electrons.gap,
                           scissor_shift))
        self.eL -= scissor_shift
        self.scissor_shift = scissor_shift
        self._logger.info("Electron data gathered.")

        self._logger.info("Energy broadening = %s eV" % alpha)
        self._logger.info("Laser energy = %s eV" %
                          (self.eL + self.scissor_shift))
        self.phonon_branches = phonon_branches
        pb = phonon_branches
        c23 = compute_second_third
        ro = reduced_only

        compute_everything = True
        self.first_resonance = None
        self.second_resonance = None
        self.third_resonance = None
        if save_resonance is not None:
            # check if there is a need to compute resonances
            self._logger.info("Path given, try to load data...")
            # try to load data
            ResonanceSaver(save_resonance, self, 
                           load_only_first=not compute_second_third,
                           loglevel=loglevel)
            if self.first_resonance is not None:
                self._logger.info("Data loaded, do not compute resonances.")
                compute_everything = False
                self.interactions = tuple(self.second_resonance.keys())
            else:
                self._logger.info("No data found, compute data then save it.")
        if compute_everything:
            if interactions == "all":
                interactions = ALL_INTERACTIONS
            elif isinstance(interactions, str):
                # user forgot to specify a tuple
                interactions = (interactions, )
            self.interactions = interactions
            f, s, t = self._get_resonances(alpha, interactions,
                                           reduced_only=ro,
                                           phonon_branches=pb,
                                           compute_second_third=c23,
                                           slow_paral=slow_paral,
                                           kplusq=kplusq)
            f, s, t = self.rearrange_data(f, s, t, compute_second_third=c23)
            (self.first_resonance,
             self.second_resonance,
             self.third_resonance) = f, s, t
            # save data to files
            if save_resonance is not None:
                self._logger.info("Saving data...")
                ResonanceSaver(save_resonance, self, loglevel=loglevel)

    def rearrange_data(self, first_resonance, second_resonance,
                       third_resonance, compute_second_third=True):
        """Method that reorganize data into a simpler way.

        This rearrangement will reduce greatly the number of dictionaries
        and thus, decrease memory consumption.
        """
        self._logger.info("Rearranging data for better memory consumption.")
        # first resonance
        fkeys = list(first_resonance[0].keys())
        f = {}
        for k in fkeys:
            f[k] = np.array([x[k] for x in first_resonance])
        first_resonance = f
        if not compute_second_third:
            return first_resonance, [], []
        # second and third resonance
        skeys = list(second_resonance.keys())
        ssubkeys = list(second_resonance[skeys[0]][0][0].keys())
        s, t = {}, {}
        for i in skeys:
            s[i] = {}
            t[i] = {}
            for k in ssubkeys:
                group2 = []
                group3 = []
                for sublist2, sublist3 in zip(second_resonance[i],
                                              third_resonance[i]):
                    group2 += [x[k] for x in sublist2]
                    group3 += [x[k] for x in sublist3]
                s[i][k] = np.array(group2)
                t[i][k] = np.array(group3)
        second_resonance = s
        third_resonance = t
        return first_resonance, second_resonance, third_resonance

    def _get_resonances(self, alpha, interactions,
                        compute_second_third=True, **kwargs):

        ph_br = kwargs.pop("phonon_branches", (6, 10))
        slow_paral = kwargs.pop("slow_paral")
        kplusq = kwargs.pop("kplusq", 1.0)
        # first denom is independant of interaction
        first = self._get_first_resonance(alpha,
                                          **kwargs)
        if not compute_second_third:
            self._logger.info("Do not compute phonons/defect resonances.")
            d = {key: [[], ] * len(first) for key in interactions}
            return first, d, d
        self._logger.info("Interactions to compute: %s" % str(interactions))
        # data dictionaries
        data2 = {}
        data3 = {}
        for interaction in interactions:
            self._logger.info("Computing interaction: %s" % interaction)
            if interaction not in ALL_INTERACTIONS:
                raise ValueError("Not a valid interaction: %s" % interaction)
            # check interaction order
            if interaction[-1] == "1":
                # phonon interact first then defect
                phonon2 = True
                phonon3 = True
                if interaction[0] == "e" and interaction[1] == "e":
                    # first and second particle to interact is electron
                    electron_qpt_sign2 = 1
                    hole_qpt_sign2 = 0
                    electron_qpt_sign3 = 0
                    hole_qpt_sign3 = 0
                elif interaction[0] == "h" and interaction[1] == "e":
                    # first particle to interact is hole second is electron
                    electron_qpt_sign2 = 0
                    hole_qpt_sign2 = -1
                    electron_qpt_sign3 = -1
                    hole_qpt_sign3 = -1
                elif interaction[0] == "e" and interaction[1] == "h":
                    # first particle to interact is electron second is hole
                    electron_qpt_sign2 = 1
                    hole_qpt_sign2 = 0
                    electron_qpt_sign3 = 1
                    hole_qpt_sign3 = 1
                elif interaction[0] == "h" and interaction[1] == "h":
                    # first and second particle to interact is hole
                    electron_qpt_sign2 = 0
                    hole_qpt_sign2 = -1
                    electron_qpt_sign3 = 0
                    hole_qpt_sign3 = 0
            else:
                # defect first then phonon
                phonon2 = False
                phonon3 = True
                if interaction[0] == "e" and interaction[1] == "e":
                    # first and second particle to interact is electron
                    electron_qpt_sign2 = -1
                    hole_qpt_sign2 = 0
                    electron_qpt_sign3 = 0
                    hole_qpt_sign3 = 0
                elif interaction[0] == "h" and interaction[1] == "e":
                    # first particle to interact is hole second is electron
                    electron_qpt_sign2 = 0
                    hole_qpt_sign2 = 1
                    electron_qpt_sign3 = 1
                    hole_qpt_sign3 = 1
                elif interaction[0] == "e" and interaction[1] == "h":
                    # first particle to interact is electron second is hole
                    electron_qpt_sign2 = -1
                    hole_qpt_sign2 = 0
                    electron_qpt_sign3 = -1
                    hole_qpt_sign3 = -1
                elif interaction[0] == "h" and interaction[1] == "h":
                    # first and second particle to interact is hole
                    electron_qpt_sign2 = 0
                    hole_qpt_sign2 = 1
                    electron_qpt_sign3 = 0
                    hole_qpt_sign3 = 0
            second = self._get_phonon_resonance(alpha, first, phonon=phonon2,
                                                el_qpt_sign=electron_qpt_sign2,
                                                hole_qpt_sign=hole_qpt_sign2,
                                                phonon_branches=ph_br,
                                                slow_paral=slow_paral,
                                                kplusq=kplusq)
            third = self._get_phonon_resonance(alpha, first, phonon=phonon3,
                                               el_qpt_sign=electron_qpt_sign3,
                                               hole_qpt_sign=hole_qpt_sign3,
                                               phonon_branches=ph_br,
                                               slow_paral=slow_paral,
                                               kplusq=kplusq)
            data2[interaction] = second
            data3[interaction] = third
        return first, data2, data3

    def _get_phonon_resonance(self, alpha, first_resonance, phonon=True,
                              el_qpt_sign=1, hole_qpt_sign=1,
                              phonon_branches=(6, 10),
                              slow_paral=False,
                              kplusq=None):
        self._logger.info("Getting Phonon resonance.")
        self._logger.info("Phonon branches to consider: %s" %
                          str(phonon_branches))
        # get the second resonant term when the first one is resonant.
        # this is to keep the search limited
        # resonant kpts, energies and conduction bands
        # qsign is the sign of the q in energy

        # ## regroup data for computation of phonon resonance
        eL = self.eL
        kpts = np.array([x["kpt"] for x in first_resonance])  # resonant kpts
        # get max and mins of resonant kpt vectors
        if kplusq is not None:
            allkx = kpts[:, 0]
            allky = kpts[:, 1]
            maxx = np.max(allkx) * kplusq
            minx = np.min(allkx) * kplusq
            maxy = np.max(allky) * kplusq
            miny = np.min(allky) * kplusq
        else:
            maxx = 0.5
            minx = -0.5
            maxy = 0.5
            miny = -0.5
        ph_e = self.phonons.data["frequencies"]
        qpts = self.phonons.data["qpts"]
        all_kpts = self.electrons.data["kpts"]
        el_energies = self.electrons.data["energies"]
        resonance = []
        # shift phonon energies (because -q)
        wqnu_list = ph_e
        # start multiprocessing
        pool = Pool(self.paral_nproc)
        results = []
        niter = len(kpts)
        self._logger.info("Submitting jobs in parallel...")
        for ik, res in enumerate(first_resonance):
            if slow_paral:
                if (not ik % self.paral_nproc and ik > 0) or ik + 1 == niter:
                    resonance += self._get_pool_results(results, verbose=False)
                    self._logger.info("Progress: %.1f %%" % (ik / niter * 100))
            else:
                # launch everything and get results after
                pass
            # TODO: clean up the arguments of this method. jesus :S
            args = [res["kpt"], res["eigenvalues"], res["c"], res["v"],
                    qpts, el_qpt_sign,
                    hole_qpt_sign, all_kpts,
                    phonon,
                    wqnu_list, phonon_branches,
                    alpha, eL, el_energies,
                    maxx, maxy, minx, miny]
            # using apply_async so that every call is independant of each other
            results.append(pool.apply_async(get_phonon_subres, args))
            # ## GET RESULTS AFTER NPROC SUBMITS
            # this is to not clog up the ram (wait before continuing)
            # the method below clears the processes
        # once all jobs submitted, close the pool and wait for results
        pool.close()
        if not slow_paral:
            # get everything
            resonance += self._get_pool_results(results, verbose=True)

        ntotal = 0
        for subresonance in resonance:
            ntotal += len(subresonance)
        self._logger.info("Phonon resonance done. N=%s" % ntotal)
        return resonance

    def _get_pool_results(self, submits, verbose=True):
        results = []
        N = len(submits)
        fivepercent = int(N / 20)
        if not fivepercent:
            fivepercent = 1
        for i, submit in enumerate(submits):
            if verbose and not i % fivepercent and i:
                # show progress every ~ 5%
                self._logger.info("Progress : %.1f / 100.0" % (i / N * 100))
            results.append(submit.get())
        submits.clear()
        return results

    def _get_first_resonance(self, alpha, reduced_only=True):
        self._logger.info("Getting First Resonances.")
        eL = self.eL
        e = self.electrons.data["energies"]  # must be in eV
        kpts = self.electrons.data["kpts"]
        resonance = []
        if reduced_only:
            # compute electron-light resonance only in reduced BZ
            self._logger.info("First resonances only in 1st quadrant of BZ.")
            reduced_kpts = self._get_reduced_bz_kpts(kpts)
            reduced_index = find_map_sorted(reduced_kpts, kpts)
            e = e[reduced_index]
            kpts = reduced_kpts.copy()  # rename variable to be usable

        allowed_transitions = self.electrons.data["allowed_transitions"]
        for i, transition in enumerate(allowed_transitions):
            # compute energy difference between whole bands
            diff = np.abs(np.subtract(e[:, transition[0]] + eL,
                                      e[:, transition[1]]))
            interest = np.where(diff <= alpha)[0]
            for n in interest:
                resonance.append({"kpt": kpts[n],
                                  "c": transition[1],
                                  "v": transition[0],
                                  "eck": e[n, transition[1]],
                                  "evk": e[n, transition[0]],
                                  "eigenvalues": e[n, :],
                                  # "index": n
                                  })
            # check advancement
            self._logger.info("Progress : %.1f" %
                              ((i + 1) / len(allowed_transitions) * 100))
        self._logger.info("First resonance done. N=%i" % len(resonance))
        return resonance

    def _get_reduced_bz_kpts(self, kpts_list):
        # get only the kpts which are inside the first quadrant
        # i.e.: 0<= kx, ky <= 0.5
        l = []
        for kpt in kpts_list:
            if kpt[0] >= 0.0 and kpt[1] >= 0.0:
                l.append(kpt)
        return np.array(l)
