import os
import logging
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from bpproject import EV_TO_NM as eV_TO_nm


class ResonanceMapper:
    def __init__(self, checker, loglevel=logging.INFO):
        # setup logger
        logging.basicConfig()
        self._logger = logging.getLogger("Mapper")
        self._logger.setLevel(loglevel)

        self.checker = checker

    def el_dispersion_2D(self, show=True, save=None, filename=None):
        laser = eV_TO_nm / (self.checker.eL + self.checker.scissor_shift)
        fig = plt.figure()
        fig2 = plt.figure()
        marks_at = self.checker.first_resonance["kpt"]
        kx = marks_at[:, 0]
        ky = marks_at[:, 1]
        el_energies = self.checker.first_resonance["eck"]
        h_energies = self.checker.first_resonance["evk"]
        sortel = np.argsort(el_energies)
        sorth = np.argsort(h_energies)
        kxel = kx[sortel]
        kyel = ky[sortel]
        el = (el_energies[sortel] - self.checker.electrons.fermi_level +
              self.checker.scissor_shift)
        kxh = kx[sorth]
        kyh = ky[sorth]
        h = h_energies[sorth] - self.checker.electrons.fermi_level
        nres = len(kx)
        cmap = mpl.colors.LinearSegmentedColormap.from_list('mycolors',
                                                            ['blue', 'red'])
        colors = [cmap(i / nres) for i in range(nres)]
        # dummy plot for colorbar
        ax = fig.add_subplot(111)
        CS3 = ax.contourf([[0, 0], [0, 0]], np.unique(el), cmap=cmap)
        fig.clf()

        ax2 = fig2.add_subplot(111)
        CS4 = ax2.contourf([[0, 0], [0, 0]], np.unique(h), cmap=cmap)
        fig2.clf()
        # electrons
        ax = fig.add_subplot(111)
        for x, y, color in zip(kxel, kyel, colors):
            ax.plot(x, y, markersize=5,
                    marker="D", linestyle="None", color=color)
        ax.set_xlabel("zigzag direction")
        ax.set_ylabel("armchair direction")
        ax.set_xlim([-0.6, 0.6])
        ax.set_ylim([-0.6, 0.6])
        ax.set_title("Electron energy 1st resonance,"
                     " laser=%.1f nm, broad=%s eV" %
                     (laser, self.checker.alpha))
        ax.hlines((-0.5, 0.5), -0.5, 0.5, color="k")
        ax.vlines((-0.5, 0.5), -0.5, 0.5, color="k")
        ax.axhline(0.0, linestyle=":", color="k")
        ax.axvline(0.0, linestyle=":", color="k")
        fig.colorbar(CS3)

        # holes
        ax2 = fig2.add_subplot(111)
        for x, y, color in zip(kxh, kyh, colors):
            ax2.plot(x, y, marker="D", linestyle="None", color=color)
        ax2.set_xlim([-0.6, 0.6])
        ax2.set_ylim([-0.6, 0.6])
        ax2.set_xlabel("zigzag direction")
        ax2.set_ylabel("armchair direction")
        ax2.set_title("Hole energy 1st resonance,"
                      " laser=%.1f nm, broad=%s eV" %
                      (laser, self.checker.alpha))
        ax2.hlines((-0.5, 0.5), -0.5, 0.5, color="k")
        ax2.vlines((-0.5, 0.5), -0.5, 0.5, color="k")
        ax2.axhline(0.0, linestyle=":", color="k")
        ax2.axvline(0.0, linestyle=":", color="k")
        fig2.colorbar(CS4)

        # show and save figures
        if show:
            plt.show()
        if save is not None:
            if filename is None:
                filename = "first_resonance_electron_2Ddispersion.png"
                filenameh = "first_resonance_hole_2Ddispersion.png"
            fig.savefig(os.path.join(save, filename))
            fig2.savefig(os.path.join(save, filenameh))

    def el_dispersion(self, show=True, save=None, filename=None):
        laser = self.checker.eL + self.checker.scissor_shift
        fig = plt.figure()
        ax = fig.add_subplot(111)
        el = self.checker.first_resonance["eck"]
        h = self.checker.first_resonance["evk"]
        ax.plot(el, label="Conduction")
        ax.plot(h, label="Valence")
        ax.set_ylabel("Energy (eV)")
        ax.set_title("Electron energy 1st resonance,"
                     " laser=%.1f nm, broad=%s eV" %
                     (laser, self.checker.alpha))
        for i in (max(el), min(el), max(h), min(h)):
            ax.axhline(i, linestyle=":", color="k")
        plt.legend(loc="best")
        if show:
            plt.show()
        if save is not None:
            if filename is None:
                filename = "first_resonance_electron_dispersion.png"
            fig.savefig(os.path.join(save, filename))
        plt.close(fig)

    def map_all_resonance(self, only_all=False, **kwargs):
        # one by one
        save = kwargs.pop("save", None)
        if not only_all:
            for i, interaction in enumerate(self.checker.interactions):
                for branch in self.checker.phonon_branches:
                    subpath = os.path.join(save, "b%i" % branch)
                    if not os.path.isdir(subpath):
                        os.mkdir(subpath)
                    if i:
                        self.map_resonance(interaction, branch, save=subpath,
                                           first=False, **kwargs)
                    else:
                        self.map_resonance(interaction, branch, save=subpath,
                                           first=True, **kwargs)
                self.map_resonance(interaction, "all", save=save,
                                   first=False, **kwargs)
        # all in one
        self.map_all_in_one("all", "all", save=save, **kwargs)

    def map_all_in_one(self, interactions, branches, legend=True, show=True,
                       save=None,
                       filename=None):
        if branches == "all":
            branches = self.checker.phonon_branches
        if isinstance(branches, int):
            branches = (branches, )
        for branch in branches:
            if branch not in self.checker.phonon_branches:
                self._logger.critical("%s not computed in phonons branches!" %
                                      branch)
                return
        if interactions == "all":
            interactions = self.checker.interactions
        fig2 = plt.figure()
        fig3 = plt.figure()
        ax2 = fig2.add_subplot(111)
        ax3 = fig3.add_subplot(111)
        laser = eV_TO_nm / (self.checker.eL + self.checker.scissor_shift)
        cmap = plt.get_cmap("jet")

        # draw BZ limits
        for a in (ax2, ax3):
            a.set_ylim((-0.6, 0.6))
            a.set_xlim((-0.6, 0.6))
            a.hlines((-0.5, 0.5), -0.5, 0.5, colors="k")
            a.vlines((-0.5, 0.5), -0.5, 0.5, colors="k")
            a.axvline(0.0, linestyle=":", color="k")
            a.axhline(0.0, linestyle=":", color="k")
            a.set_xlabel(r"$q_x$")
            a.set_ylabel(r"$q_y$")
        ax2.set_title("Second resonance map, laser=%.1f nm, broad=%s eV,"
                      " all interactions, branch=%s" %
                      (laser, self.checker.alpha, str(branches)))
        ax3.set_title("Third resonance map, laser=%.1f nm, broad=%s eV,"
                      " all interactions, branch=%s" %
                      (laser, self.checker.alpha, str(branches)))
        nint = len(interactions)
        colors = [cmap(i / nint) for i in range(nint)]
        # get data
        r2 = self.checker.second_resonance
        r3 = self.checker.third_resonance
        for i, interaction in enumerate(interactions):
            qpts2 = []
            qpts3 = []
            for branch in branches:
                where2 = np.where(r2[interaction]["branch"] == branch)[0]
                qpts2 += r2[interaction]["qpt"][where2].tolist()
                where3 = np.where(r3[interaction]["branch"] == branch)[0]
                qpts3 += r3[interaction]["qpt"][where3].tolist()
            qpts2 = np.array(qpts2)
            qpts3 = np.array(qpts3)
            if len(qpts2.shape) == 1:
                qx2 = []
                qy2 = []
            else:
                qx2 = qpts2[:, 0]
                qy2 = qpts2[:, 1]
            label = interaction
            ax2.plot(qx2, qy2, marker="D", linestyle="None",
                     color=colors[i], label=label)
            if len(qpts3.shape) == 1:
                qx3 = []
                qy3 = []
            else:
                qx3 = qpts3[:, 0]
                qy3 = qpts3[:, 1]
            ax3.plot(qx3, qy3, marker="D", linestyle="None",
                     color=colors[i], label=label)
        if legend:
            handles, labels = ax2.get_legend_handles_labels()
            lgd2 = ax2.legend(handles, labels, loc='upper center',
                              bbox_to_anchor=(1.1, 1.0))
            handles, labels = ax3.get_legend_handles_labels()
            lgd3 = ax3.legend(handles, labels, loc='upper center',
                              bbox_to_anchor=(1.1, 1.0))
        if show:
            plt.show()
        if save is not None:
            if filename is None:
                filename = ("_resonance_map_laser%.2f_broad%s_interactionall_"
                            % (self.checker.eL + self.checker.scissor_shift,
                               self.checker.alpha))
                for branch in branches:
                    filename += "b" + str(branch)
                filename += ".png"
            fig2name = "second" + filename
            fig3name = "third" + filename
            path2 = os.path.join(save, fig2name)
            path3 = os.path.join(save, fig3name)
            fig2.savefig(path2, bbox_extra_artists=(lgd2,),
                         bbox_inches='tight')
            fig3.savefig(path3, bbox_extra_artists=(lgd3,),
                         bbox_inches='tight')
        plt.close(fig2)
        plt.close(fig3)

    def map_resonance(self, interaction, branches, legend=False, show=True,
                      save=None, filename=None, first=True):
        laser = eV_TO_nm / (self.checker.eL + self.checker.scissor_shift)
        if branches == "all":
            branches = self.checker.phonon_branches
        if isinstance(branches, int):
            branches = (branches, )
        for branch in branches:
            if branch not in self.checker.phonon_branches:
                self._logger.critical("%s not computed in phonons branches!" %
                                      branch)
                return
        if interaction not in self.checker.interactions:
            self._logger.critical("%s not in computed interactions!" %
                                  interaction)
            return
        if first:
            fig = plt.figure()
            ax = fig.add_subplot(111)
            ax.set_title(r"First resonance map $\lambda_L=$ %.1f (nm),"
                         " broad=%s eV" % (laser, self.checker.alpha))
            ax.set_ylabel(r"$k_y$")
            ax.set_xlabel(r"$k_x$")
            # draw BZ limits
            ax.set_ylim((-0.6, 0.6))
            ax.set_xlim((-0.6, 0.6))
            ax.hlines((-0.5, 0.5), -0.5, 0.5, colors="k")
            ax.vlines((-0.5, 0.5), -0.5, 0.5, colors="k")
            ax.axvline(0.0, linestyle=":", color="k")
            ax.axhline(0.0, linestyle=":", color="k")
        fig2 = plt.figure()
        fig3 = plt.figure()
        cmap = plt.get_cmap("jet")
        # first resonance
        marks_at = self.checker.first_resonance["kpt"]
        kx = marks_at[:, 0]
        ky = marks_at[:, 1]
        nresonances = len(kx)
        colors = [cmap(i / nresonances) for i in range(nresonances)]
        # second resonance
        ax2 = fig2.add_subplot(111)
        ax2.set_title("Second resonance map, laser=%.1f nm, broad=%s eV,"
                      " interaction=%s, branches=%s" %
                      (laser, self.checker.alpha, interaction, str(branches)))
        ax2.set_xlabel(r"$q_x$")
        ax2.set_ylabel(r"$q_y$")
        # draw BZ limits
        ax2.set_ylim((-0.6, 0.6))
        ax2.set_xlim((-0.6, 0.6))
        ax2.hlines((-0.5, 0.5), -0.5, 0.5, colors="k")
        ax2.vlines((-0.5, 0.5), -0.5, 0.5, colors="k")
        ax2.axvline(0.0, linestyle=":", color="k")
        ax2.axhline(0.0, linestyle=":", color="k")

        # third resonance
        ax3 = fig3.add_subplot(111)
        ax3.set_title("Third resonance map, laser=%.1f, broad=%s,"
                      " interaction=%s, branches=%s" %
                      (laser, self.checker.alpha, interaction, str(branches)))
        ax3.set_xlabel(r"$q_x$")
        ax3.set_ylabel(r"$q_y$")
        # draw BZ limits
        ax3.set_ylim((-0.6, 0.6))
        ax3.set_xlim((-0.6, 0.6))
        ax3.hlines((-0.5, 0.5), -0.5, 0.5, colors="k")
        ax3.vlines((-0.5, 0.5), -0.5, 0.5, colors="k")
        ax3.axvline(0.0, linestyle=":", color="k")
        ax3.axhline(0.0, linestyle=":", color="k")

        if first:
            for i, (kxi, kyi) in enumerate(zip(kx, ky)):
                ax.plot(kxi, kyi, marker="D", color=colors[i])
        r2 = self.checker.second_resonance
        r3 = self.checker.third_resonance
        qpts2 = []
        qpts3 = []
        for branch in branches:
            where2 = np.where(r2[interaction]["branch"] == branch)[0]
            qpts2 += r2[interaction]["qpt"][where2].tolist()
            where3 = np.where(r3[interaction]["branch"] == branch)[0]
            qpts3 += r3[interaction]["qpt"][where3].tolist()
        qpts2 = np.array(qpts2)
        qpts3 = np.array(qpts3)
        if len(qpts2.shape) == 1:
            qx2, qy2 = [], []
        else:
            qx2 = qpts2[:, 0]
            qy2 = qpts2[:, 1]
        ax2.plot(qx2, qy2, marker="D", linestyle="None")
        if len(qpts3.shape) == 1:
            qx3, qy3 = [], []
        else:
            qx3 = qpts3[:, 0]
            qy3 = qpts3[:, 1]
        ax3.plot(qx3, qy3, marker="D", linestyle="None")
        if show:
            plt.show()
        if legend:
            ax.legend(loc="best")
            ax2.legend(loc="best")
            ax3.legend(loc="best")
        if save is not None:
            if filename is None:
                filename = ("_resonance_map_laser%.2f_broad%s_interaction%s_" %
                            (self.checker.eL + self.checker.scissor_shift,
                             self.checker.alpha, interaction))
                for branch in branches:
                    filename += "b" + str(branch)
                filename += ".png"
            fig1name = "first" + filename
            fig2name = "second" + filename
            fig3name = "third" + filename
            path1 = os.path.join(save, fig1name)
            path2 = os.path.join(save, fig2name)
            path3 = os.path.join(save, fig3name)
            if first:
                fig.savefig(path1)
            fig2.savefig(path2)
            fig3.savefig(path3)
        if first:
            plt.close(fig)
        plt.close(fig2)
        plt.close(fig3)
