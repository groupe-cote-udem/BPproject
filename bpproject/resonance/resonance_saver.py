import logging
import os
import json
import numpy as np


class ResonanceSaver:
    def __init__(self, path, checker,
                 load_only_first=False,
                 loglevel=logging.INFO):
        """Objects that saves the resonances stored into the
        resonancechecker into a file. Or, it can load the resonances from
        a file and put it as the attributes of a resonancechecker.

        Parameters
        ----------
        path : str
               path of the directory containing the files of the resonances.
        checker : ResonanceChecker object
                  The ResonanceChecker object to use.
        loglevel : int, optional
                   The level for the logger object.
        load_only_first : bool, optional
                          If True, only try to read the first resonances.
        """
        logging.basicConfig()
        self._logger = logging.getLogger("Saver")
        self._logger.setLevel(loglevel)
        self.checker = checker
        lof = load_only_first       
        if checker.first_resonance is None:
            # we need to load data
            (checker.first_resonance,
             checker.second_resonance,
             checker.third_resonance) = self.load_data_from_file(path,
                                                                 lof)
        else:
            # we need to store data to a file
            self.save_data_to_file(path, checker.first_resonance,
                                   checker.second_resonance,
                                   checker.third_resonance)

    def save_data_to_file(self, path, first_resonance,
                          second_resonance, third_resonance,
                          overwrite=False):
        first = os.path.join(path, "first_resonance.data")
        second = os.path.join(path, "second_resonance.data")
        third = os.path.join(path, "third_resonance.data")
        # check path to make sure no data is overwritten / lost
        if self._check_if_file_exists(first, second, third):
            if overwrite:
                # delete previous files
                for fichier in (first, second, third):
                    os.remove(fichier)
            else:
                msg = ("Resonance files already exists! Change directory"
                       " or move the files.")
                raise ValueError(msg)
        if not os.path.isdir(path):
            os.mkdir(path)

        with open(first, "w") as f:
            json.dump(first_resonance, f, cls=NumpyAwareJSONEncoder)
            self._logger.info("Saved first resonance to: %s" % first)
        with open(second, "w") as f:
            json.dump(second_resonance, f, cls=NumpyAwareJSONEncoder)
            self._logger.info("Saved second resonance to: %s" % second)
        with open(third, "w") as f:
            json.dump(third_resonance, f, cls=NumpyAwareJSONEncoder)
            self._logger.info("Saved third resonance to: %s" % third)

    def load_data_from_file(self, path, load_only_first):
        first = os.path.join(path, "first_resonance.data")
        second = os.path.join(path, "second_resonance.data")
        third = os.path.join(path, "third_resonance.data")
        if not self._check_if_file_exists(first, second, third):
            # no data to load
            return None, None, None

        V2first = V2_NumpyAwareJSONDecoderFirstRes
        V2_23 = V2_NumpyAwareJSONDecoderSecondThirdRes
        V1first = V1_NumpyAwareJSONDecoderFirstRes
        V1_23 = V1_NumpyAwareJSONDecoderSecondThirdRes
        try:
            self._logger.info("Loading data...")
            f = self._do_loading(first, V2first)
            if load_only_first:
                return f, {}, {}
            s = self._do_loading(second, V2_23)
            t = self._do_loading(third, V2_23)
        except:
            try:
                self._logger.info("V2 loading failed, try V1...")
                f = self._do_loading(first, V1first)
                if not load_only_first:
                    s = self._do_loading(second, V1_23)
                    t = self._do_loading(third, V1_23)
                else:
                    s = []
                    t = []
            except:
                raise
            else:
                self._logger.info("V1 worked. Reformat data, and resave.")
                c23 = not load_only_first
                f, s, t = self.checker.rearrange_data(f, s, t,
                                                      compute_second_third=c23)
                self.save_data_to_file(path, f, s, t, overwrite=True)
        return f, s, t

    def _do_loading(self, path, decoderclass):
        with open(path, "r") as f:
            resonance = json.load(f, cls=decoderclass)
        return resonance

    def _check_if_file_exists(self, *args):
        for path in args:
            if os.path.isfile(path):
                # one file already present
                return True
        return False


class V2_NumpyAwareJSONDecoderSecondThirdRes(json.JSONDecoder):
    def decode(self, obj):
        obj = super().decode(obj)
        for interaction, d in obj.items():
            for k, liste in d.items():
                if isinstance(liste, dict):
                    # this list is supposed to be a numpy array
                    obj[interaction][k] = np.array(liste["data"])
        return obj


class V2_NumpyAwareJSONDecoderFirstRes(json.JSONDecoder):
    def decode(self, obj):
        obj = super().decode(obj)
        for k, v in obj.items():
            if isinstance(v, dict):
                obj[k] = np.array(v["data"])
        return obj


class V1_NumpyAwareJSONDecoderSecondThirdRes(json.JSONDecoder):
    def decode(self, obj):
        obj = super().decode(obj)
        # second/third res are organised alongside
        for interaction, allres in obj.items():
            for res in allres:
                for subres in res:
                    for k, v in subres.items():
                        if isinstance(v, dict):
                            subres[k] = np.array(v["data"])
        return obj


class V1_NumpyAwareJSONDecoderFirstRes(json.JSONDecoder):
    def decode(self, obj):
        obj = super().decode(obj)
        # here, first resonance is a list of resonance
        for res in obj:
            for k, v in res.items():
                if isinstance(v, dict):
                    # numpy array (since there is no other dict elsewhere)
                    res[k] = np.array(v["data"])
        return obj


class NumpyAwareJSONEncoder(json.JSONEncoder):
    """A Json Encoder that can encode numpy arrays.
    """
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return {"__numpy__": "array", "data": obj.tolist()}
        return obj
