import numpy as np
import numpy_indexed as npi

# I put these methods here because I need them to be outside a class
# so that it is easier to use in parallel computing.


def get_phonon_subres(kpt, eigens, i_cond, i_val, qpts, el_qpt_sign,
                      hole_qpt_sign, all_kpts, phonon,
                      wqnu_list, phonon_branches,
                      alpha, eL, el_energies,
                      maxx, maxy, minx, miny):
    subresonance = []
    # Looking for subresonance
    # final electron kpt
    if el_qpt_sign:
        kqel = el_qpt_sign * qpts + kpt
    else:
        kqel = np.array([kpt.tolist(), ])
    # final hole kpt
    if hole_qpt_sign:
        kqh = hole_qpt_sign * qpts + kpt
    else:
        kqh = np.array([kpt.tolist(), ])
    # check that final kpts are still in 1st BZ
    kqel = _stay_in_zb(kqel)
    kqh = _stay_in_zb(kqh)

    ########################################################################
    # drop transitions that are too far (to prevent jump outside the first #
    # resonant region). This is to make sure no forbidden transitions are  #
    # done since electrons and holes should always approach Gamma and not  #
    # go to another valley                                                 #
    ########################################################################

    # this should considerably accelerate the computation
    if el_qpt_sign or hole_qpt_sign:
        wherelegitx = (minx <= kqel[:, 0]) * (maxx >= kqel[:, 0])
        wherelegity = (miny <= kqel[:, 1]) * (maxy >= kqel[:, 1])
        el_legit = wherelegitx * wherelegity
        kqel = kqel[el_legit]
        wherelegitx = (minx <= kqh[:, 0]) * (maxx >= kqh[:, 0])
        wherelegity = (miny <= kqh[:, 1]) * (maxy >= kqh[:, 1])
        h_legit = wherelegitx * wherelegity
        kqh = kqh[h_legit]

    # get considered phonons
    if el_qpt_sign and not hole_qpt_sign:
        # size of phonon grid determined by electrons
        considered_qpts = qpts[el_legit]
        # reshape holes
        l = len(kqel)
        kqh = np.reshape(np.tile(kqh[0], l), (l, len(kqh[0])))
    elif hole_qpt_sign and not el_qpt_sign:
        # by holes
        considered_qpts = qpts[h_legit]
        # reshape electrons
        l = len(kqh)
        kqel = np.reshape(np.tile(kqel[0], l), (l, len(kqel[0])))
    elif hole_qpt_sign and el_qpt_sign:
        # both phonon and electrons affected => choose electrons
        # because that's what YODA would have chosen since both electrons
        # and holes shift by the same q (should be the same arrays)
        considered_qpts = qpts[el_legit]
    elif not hole_qpt_sign and not el_qpt_sign:
        # electrons and holes not diffused: its a third resonance and they
        # will recombine after this diffusion. We must consider the whole
        # qpt grid with a global mask such that k + q is still inside
        # good region
        new_grid = _stay_in_zb(kpt + qpts)
        wherelegitx = (minx <= new_grid[:, 0]) * (maxx >= new_grid[:, 0])
        wherelegity = (miny <= new_grid[:, 1]) * (maxy >= new_grid[:, 1])
        legit = wherelegitx * wherelegity
        considered_qpts = qpts[legit]
        l = len(considered_qpts)
        kqel = np.reshape(np.tile(kqel[0], l), (l, len(kqel[0])))
        kqh = np.reshape(np.tile(kqh[0], l), (l, len(kqh[0])))
    # get new hole energies and electron energies
    # ##########################################
    if el_qpt_sign:
        # electron momentum changed => compute the shifted grid
        # index_el = [np.where((all_kpts == x).all(axis=1)) for x in kqel]
        index_el = find_map_sorted(kqel, all_kpts)
        el_shifted = el_energies[index_el]
    else:
        # electron momentum unchanged, just keep the same eigenvalues
        # for each qpt in qptsgrid
        l = len(kqel)
        el_shifted = np.reshape(np.tile(eigens, l), (l, len(eigens)))
    if hole_qpt_sign:
        # hole momentum has been shifted
        # index_h = [np.where((all_kpts == x).all(axis=1)) for x in kqh]
        index_h = find_map_sorted(kqh, all_kpts)
        h_shifted = el_energies[index_h]
    else:
        # hole momentum unchanged
        l = len(kqh)
        h_shifted = np.reshape(np.tile(eigens, l), (l, len(eigens)))
    if phonon:
        # hole or electron diffused, apply global mask, only few qpts
        # considered. Find what energy they have
        index_ph = find_map_sorted(considered_qpts, qpts)
        wqnu = wqnu_list[index_ph]
    else:
        # for energy conservation, a defect is like a phonon of zero energy
        wqnu = ZerosArray()
    for branch in phonon_branches:
        diff = np.abs(eL - el_shifted[:, i_cond] +
                      h_shifted[:, i_val] - wqnu[:, branch])
        # here, diff is a grid of energies difference values
        # the same size as the qpt grid.
        # find where are the resonances
        where = np.where(diff <= alpha)[0]
        for i in where:
            # transfer to tuple to access array
            if phonon:
                append_ph_energy = wqnu[i, branch]
            else:
                # defect: append phonon energy of the corresponding
                # qpts
                append_ph_energy = wqnu_list[i, branch]
            # append phonon energy, branch and qpt
            subresonance.append({"qpt": considered_qpts[i], "branch": branch,
                                 "wqnu": append_ph_energy,
                                 "kqel": kqel[i],
                                 "kqh": kqh[i],
                                 "c": i_cond,
                                 "v": i_val,
                                 "e_el": el_shifted[i, i_cond],
                                 "h_el": h_shifted[i, i_val]})
    return subresonance


def _stay_in_zb(kpt):
    # here, kpt is an array of shifted kpts.
    # make those who are too low come back into 1st ZB
    kpt[kpt <= -0.5] += 1.0
    # same for those who are too high
    kpt[kpt > 0.5] -= 1.0
    # round result to prevent numerical errors
    return np.round(kpt, 4)


# the methods below are taken from a SO post I did:
# http://stackoverflow.com/a/42232761/6362595

def find_map_sorted(arr1, arr2):
    # find where elements of arr1 are located in arr2
    i = npi.indices(arr2, arr1, missing="mask")
    # i is a masked array here for if some elements of arr2 are not in arr1
    # those elements are masked
    # return where elements are located at from the mask
    return i
# below does not work when arrays does not start by the first element


"""
def _invperm(p):
    q = np.empty_like(p)
    q[p] = np.arange(len(p))
    return q


def _find_map(arr1, arr2):
    o1 = np.argsort(arr1)
    o2 = np.argsort(arr2)
    return o2[_invperm(o1)]


def _find_map_2d(arr1, arr2):
    o1 = np.lexsort(arr1.T)
    o2 = np.lexsort(arr2.T)
    return o2[_invperm(o1)]


def find_map_sorted(arr1, arrs=None):
    if arrs is None:
        o1 = np.lexsort(arr1.T)
        return _invperm(o1)
    # make unique-able
    rdtype = np.rec.fromrecords(arrs[:1, ::-1]).dtype
    recstack = np.r_[arrs[:, ::-1],
                     arr1[:, ::-1]].view(rdtype).view(np.recarray)
    uniq, inverse = np.unique(recstack, return_inverse=True)
    return inverse[len(arrs):]
"""


class ZerosArray:
    """Array class that takes whatever but return zeros.
    This is to prevent the creation of a big array of zeros for nothing.
    """
    def __getitem__(self, *args, **kwargs):
        return 0.0
