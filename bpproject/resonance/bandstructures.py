import numpy as np
import matplotlib.pyplot as plt


HA_TO_EV = 27.2114  # conversion factor from hartree to eV
EV_TO_CM = 8065.73


class BaseBandstructure:
    gridlabels = None
    energylabel = None

    def __init__(self, path_energies, path_grid, flatten=True):
        self.flatten = True
        self.data = self._get_data(path_grid, path_energies)
        if not flatten:
            self.unflatten_data()

    def _get_data(self, path_grid, path_energies):
        grid = self._get_grid(path_grid)
        energies = self._get_energies(path_energies)
        return {self.gridlabels[-1]: grid, self.energylabel: energies}

    def _get_grid(self, path):
        return np.loadtxt(path)

    def _get_energies(self, path):
        return np.loadtxt(path) * HA_TO_EV

    def flatten_data(self):
        if self.flatten:
            return
        self.flatten = True
        gridx = self.data[self.gridlabels[0]]
        gridy = self.data[self.gridlabels[1]]
        gridz = self.data[self.gridlabels[2]]
        energy = self.data[self.energylabel]
        flatgrid = np.vstack(np.meshgrid(gridx, gridy,
                                         gridz)).reshape(3, -1).T
        energyflat = np.reshape(energy, (len(flatgrid), energy.shape[-1]))
        self.data.update({self.gridlabels[-1]: flatgrid,
                          self.energylabel: energyflat})

    def unflatten_data(self):
        if not self.flatten:
            return
        self.flatten = False
        grid = self.gridlabels[-1]
        new = {}
        for i, gridi in enumerate(self.gridlabels[:-1]):
            new[gridi] = np.unique(self.data[grid][:, i])
        nbands = self.data[self.energylabel].shape[-1]
        new[self.energylabel] = np.reshape(self.data[self.energylabel],
                                           (len(new[self.gridlabels[0]]),
                                            len(new[self.gridlabels[1]]),
                                            len(new[self.gridlabels[2]]),
                                            nbands))
        self.data.update(new)

    def plot(self, xmarkers=None, ymarkers=None,
             units="eV", show=True, data=None):
        """Plot an example of the bandstructure on the path
        Gamma - Y - M - X - Gamma
        """
        if xmarkers is None:
            xmarkers = []
        if type(xmarkers) not in (tuple, list):
            xmarkers = (xmarkers, )
        if ymarkers is None:
            ymarkers = []
        if type(ymarkers) not in (tuple, list):
            ymarkers = (ymarkers, )
        if data is None:
            self.unflatten_data()
            data = self.data
        fig = plt.figure()
        ax = fig.add_subplot(111)
        # get the paths
        x = self.gridlabels[0]
        y = self.gridlabels[1]
        z = self.gridlabels[2]
        # gamma (0, 0, 0) - Y (0, 0.5, 0)
        wherex = np.where(data[x] == 0.0)[0]
        wherey = np.where(data[y] >= 0.0)[0]
        wherez = np.where(data[z] == 0.0)[0]
        curve1 = data[self.energylabel][wherex, wherey, wherez]
        # Y (0, 0.5, 0) - M (0.5, 0.5, 0.0)
        wherey = np.where(data[y] == 0.5)[0]
        wherex = np.where(data[x] >= 0.0)[0]
        curve2 = data[self.energylabel][wherex, wherey, wherez]
        # M (0.5, 0.5, 0.0) - X (0.5, 0.0, 0.0)
        wherex = np.where(data[x] == 0.5)[0]
        wherey = np.where(data[y] >= 0.0)[0][::-1]
        curve3 = data[self.energylabel][wherex, wherey, wherez]
        # X (0.5, 0.0, 0.0) - Gamma (0.0, 0.0, 0.0)
        wherey = np.where(data[y] == 0.0)[0]
        wherex = np.where(data[x] >= 0.0)[0][::-1]
        curve4 = data[self.energylabel][wherex, wherey, wherez]

        # plot bandstructure
        N = len(curve1[0])  # number of bands
        n1 = len(curve1)
        n2 = len(curve2)
        n3 = len(curve3)
        n4 = len(curve4)
        Nline = n1 + n2 + n3 + n4  # number of qpts in total on the path
        bandstructure = np.zeros((N, Nline))
        bandstructure[:, :n1] = curve1.transpose()
        bandstructure[:, n1:(n1 + n2)] = curve2.transpose()
        bandstructure[:, (n1 + n2):(n1 + n2 + n3)] = curve3.transpose()
        bandstructure[:, (n1 + n2 + n3):] = curve4.transpose()
        if units == "cm-1":
            bandstructure *= EV_TO_CM
        for i in range(N):
            ax.plot(range(Nline), bandstructure[i, :])

        for ymarker in ymarkers:
            ax.axvline(n1 * ymarker / 0.5, linestyle=":", color="k")
        for xmarker in xmarkers:
            ax.axvline(Nline - xmarker * n4 / 0.5, linestyle=":", color="k")
        ax.axvline(n1, linestyle="--", color="k")
        ax.axvline(n1 + n2, linestyle="--", color="k")
        ax.axvline(n1 + n2 + n3, linestyle="--", color="k")
        # ax.axhline(0, linestyle="-", color="k")
        plt.xticks([0, n1, n1 + n2, n1 + n2 + n3, n1 + n2 + n3 + n4],
                   [r"$\Gamma$", r"$Y$", r"$M$", r"$X$", r"$\Gamma$"])
        ax.set_ylabel("Energy (%s)" % units)
        ax.set_title("Bandstructure")
        if show:
            plt.show()
        return bandstructure, ax

    def _find_gamma(self):
        if not self.flatten:
            gammax = np.where(self.data[self.gridlabels[0]] == 0.0)[0][0]
            gammay = np.where(self.data[self.gridlabels[1]] == 0.0)[0][0]
            gammaz = np.where(self.data[self.gridlabels[2]] == 0.0)[0][0]
            toreturn = (gammax, gammay, gammaz)
        else:
            toreturn = np.where(np.all(self.data[self.gridlabels[-1]] ==
                                       [0.0, 0.0, 0.0], axis=1))[0][0]
        return toreturn


class ElectronsBandstructure(BaseBandstructure):
    gridlabels = ("kx", "ky", "kz", "kpts")
    energylabel = "energies"

    def _get_data(self, *args):
        data = super()._get_data(*args)
        path_energies = args[1]
        data["nband"] = data[self.energylabel].shape[-1]
        data["nvalence"] = 10
        n = data["nvalence"]
        # experimental values of bandgap
        if "bilayer" in path_energies:
            data["exp_gap"] = 1.15
            data["allowed_transitions"] = ((n - 1, n),
                                           (n - 2, n + 1),
                                           (n - 3, n + 2))
        elif "monolayer" in path_energies:
            data["exp_gap"] = 1.75  # in eV
            data["allowed_transitions"] = ((n - 1, n), )
        else:
            data["exp_gap"] = None
        # data["nvalence"] = 20  # comment this line when not inverting bands
        # even though bilayer has 10 more valence bands, the 10 lowest bands
        # are skipped when loading data.
        return data

    def _get_energies(self, path):
        data = np.loadtxt(path)
        # comment the lines below if there is band inverter is used!!!!
        if "bilayer" in path:
            # skip first 10 valence bands (too low energy for any transitions)
            data = data[:, 10:]
        return data

    @property
    def gap(self):
        if hasattr(self, "_gap"):
            return self._gap
        gamma = self._find_gamma()
        fermiband = self.data["nvalence"] - 1
        conductband = self.data["nvalence"]
        if not self.flatten:
            fermi = self.data[self.energylabel][gamma[0], gamma[1], gamma[2],
                                                fermiband]
            firstconduct = self.data[self.energylabel][gamma[0], gamma[1],
                                                       gamma[2],
                                                       conductband]
        else:
            fermi = self.data[self.energylabel][gamma, fermiband]
            firstconduct = self.data[self.energylabel][gamma, conductband]
        self._gap = firstconduct - fermi
        return self._gap

    @property
    def fermi_level(self):
        if hasattr(self, "_fermi_level"):
            return self._fermi_level
        gamma = self._find_gamma()
        fermiband = self.data["nvalence"] - 1
        if not self.flatten:
            self._fermi_level = self.data[self.energylabel][gamma[0], gamma[1],
                                                            gamma[2],
                                                            fermiband]
        else:
            self._fermi_level = self.data[self.energylabel][gamma, fermiband]
        return self._fermi_level

    def plot(self, xmarkers=None, ymarkers=None):
        self.unflatten_data()
        datatemp = self.data.copy()
        datatemp[self.energylabel] -= self.fermi_level
        bandstructure, ax = super().plot(data=datatemp,
                                         xmarkers=xmarkers,
                                         ymarkers=ymarkers, show=False)
        ax.axhline(0.0, linestyle=":", color="b")
        ax.axhline(self.gap, linestyle=":", color="b")
        ax.set_title("Electron Bandstructure, $E_{gap}=$%s, $E_{fermi}=$%s" %
                     (self.gap, self.fermi_level))
        plt.show()
        del datatemp


class PhononsBandstructure(BaseBandstructure):
    gridlabels = ("qx", "qy", "qz", "qpts")
    energylabel = "frequencies"

    def _get_grid(self, path):
        return np.loadtxt(path, usecols=(0, 1, 2))

    def plot(self, **kwargs):
        super().plot(units="cm-1", **kwargs)
