import numpy as np
import matplotlib.pyplot as plt
import os
import logging
from collections import OrderedDict
from scipy.ndimage.filters import gaussian_filter1d
from bpproject import EV_TO_CM, ALL_INTERACTIONS
from .routines import find_map_sorted


COLORS = ('b', 'r', 'g', 'c', 'm', 'y', "lime", "k", "darkorange")
ALL_MASKS = ("circle", "rectangle", "none")


class Histograms:
    def __init__(self, checker, loglevel=logging.INFO):
        logging.basicConfig()
        self._logger = logging.getLogger("Histograms")
        self._logger.setLevel(loglevel)

        self.checker = checker
        self._second_third_resonance_exists = True
        # check if second and third resonance have been computed
        i0 = checker.interactions[0]
        k0 = list(checker.second_resonance[i0].keys())[0]
        if not len(checker.second_resonance[i0][k0]):
            self._second_third_resonance_exists = False

    def plot_all_2D_qpt_histogram(self, only_all=False, **kwargs):
        path = kwargs.pop("save", None)
        if not only_all:
            for i, interaction in enumerate(self.checker.interactions):
                allqpt2Dhistpath = None
                allkplusqpath = None
                if path is not None:
                    allqpt2Dhistpath = os.path.join(path, "qpt2Dhist")
                    if not os.path.isdir(allqpt2Dhistpath):
                        os.mkdir(allqpt2Dhistpath)
                    allkplusqpath = os.path.join(path, "kplusq")
                    if not os.path.isdir(allkplusqpath):
                        os.mkdir(allkplusqpath)
                for branch in self.checker.phonon_branches:
                    subpath = None
                    kplusqpath = None
                    qpt2Dhistpath = None
                    if path is not None:
                        # create subdir for all specific phooon branches
                        subpath = os.path.join(path, "b%i" % branch)
                        if not os.path.isdir(subpath):
                            os.mkdir(subpath)
                        kplusqpath = os.path.join(subpath, "kplusq")
                        if not os.path.isdir(kplusqpath):
                            os.mkdir(kplusqpath)
                        qpt2Dhistpath = os.path.join(subpath, "qpt2Dhist")
                        if not os.path.isdir(qpt2Dhistpath):
                            os.mkdir(qpt2Dhistpath)
                    self.plot_2D_qpt_histogram(interaction, branch,
                                               save=qpt2Dhistpath,
                                               **kwargs)
                    self.plot_2D_kplusq_histogram(interaction, branch,
                                                  save=kplusqpath, **kwargs)
                    if not i:
                        # do this only once per branch (not on every interact)
                        self.plot_2D_qpt_histogram("all", branch,
                                                   save=qpt2Dhistpath,
                                                   **kwargs)
                        self.plot_2D_kplusq_histogram("all", branch,
                                                      save=kplusqpath,
                                                      **kwargs)
                # plot single interaction but all branches
                # put this in main directory
                self.plot_2D_qpt_histogram(interaction, "all",
                                           save=allqpt2Dhistpath,
                                           **kwargs)
                self.plot_2D_kplusq_histogram(interaction, "all",
                                              save=allkplusqpath,
                                              **kwargs)
        # plot all interaction with all branches
        self.plot_2D_qpt_histogram("all", "all", save=allqpt2Dhistpath,
                                   **kwargs)
        self.plot_2D_kplusq_histogram("all", "all", save=allkplusqpath,
                                      **kwargs)

    def _check_interactions_and_branches(self, interactions, branches,
                                         first_resonance_only):
        figname = None
        fro = first_resonance_only
        if interactions == "all":
            interactions = self.checker.interactions
            figname = "-".join(interactions)
            if sorted(interactions) == sorted(ALL_INTERACTIONS):
                figname = "all"
        if isinstance(interactions, str):
            interactions = (interactions, )
        for interaction in interactions:
            if interaction not in self.checker.interactions and not fro:
                self._logger.critical("%s not in computed interactions!" %
                                      interaction)
                raise ValueError()
        # at this point, if figname is None, join all interactions to compute
        if figname is None:
            figname = "-".join(interactions)

        if branches is not None:
            figname += "_"
            if branches == "all":
                branches = self.checker.phonon_branches
            if isinstance(branches, int):
                # given a simple integer
                branches = (branches, )
            for branch in branches:
                if branch not in self.checker.phonon_branches:
                    mes = "%s not in computed phonon branches!" % branch
                    self._logger.critical(mes)
                    raise ValueError(mes)
                figname += "b%i" % branch
        return interactions, branches, figname

    def plot_2D_kplusq_histogram(self, interactions, branches, filename=None,
                                 bins=None, show=True, save=None, **kwargs):
        """Method that plots a 2D histogram of the electron / hole
        k' = k +/- q after a diffusion
        """
        i, b, f = self._check_interactions_and_branches(interactions, branches,
                                                        False)
        interactions, branches, figname = i, b, f
        # get resonant k + q
        kplusqel = []
        kplusqh = []
        for resonance in (self.checker.second_resonance,
                          self.checker.third_resonance):
            for interaction in interactions:
                res = resonance[interaction]
                for branch in branches:
                    where = np.where(res["branch"] == branch)[0]
                    kplusqel += res["kqel"][where].tolist()
                    kplusqh += res["kqh"][where].tolist()
        kplusqel = np.array(kplusqel)
        kplusqh = np.array(kplusqh)

        laser = self.checker.eL + self.checker.scissor_shift
        broad = self.checker.alpha
        title = (r"$\epsilon_L$=%.2f, broad=%s, path=%s,"
                 "branch=%s" % (laser, broad, figname, str(branches)))

        if save is not None:
            if filename is None:
                filename = "kplusq_2D_hist"
                filename += ("_laser%.2f_broad%s_interaction%s" %
                             (laser, broad, figname))
                for branch in branches:
                    filename += "b" + str(branch)
                filename += ".png"
            filename1 = "el_" + filename
            filename2 = "h_" + filename
            title1 = "el " + title
            title2 = "h " + title
        else:
            title1 = None
            title2 = None
            filename1 = None
            filename2 = None
        # compute 2D histogram
        self._plot_2D_hist(kplusqel, bins=bins, title=title1, show=show,
                           save=save, filename=filename1, phonon_grid=False,
                           **kwargs)
        self._plot_2D_hist(kplusqh, bins=bins, title=title2, show=show,
                           save=save, filename=filename2, phonon_grid=False,
                           **kwargs)

    def plot_2D_qpt_histogram(self, interactions, branches,
                              show=True, save=None,
                              first_resonance_only=False, filename=None,
                              bins=None, **kwargs):
        """Method that plots and/or save a 2D histogram of resonant qpts.

        Parameters
        ----------
        interactions : tuple, list
                       Names of the interactions to consider.
        branches : tuple, list
                   Index of phonons branches to plot the histogram. If set
                   to "all", all phonons branches computed will be plotted.
        show : bool, optional
               If True, the graphs is shown.
        save : str, optional
               If not None, it must be the path to where the figure will be
               saved. Use filename to set the filename.
        filename : str, optional
                   Gives the filename of the saved figure. If None, a default
                   name will be taken.
        first_resonance_only : bool, optional
                               If True, the qpt histogram will take only the
                               qpts between the first resonant kpts.
        bins : int, optional
               Gives the number of bins on one axis for the histogram.
        """
        fro = first_resonance_only
        i, b, f = self._check_interactions_and_branches(interactions,
                                                        branches, fro)
        interactions, branches, figname = i, b, f
        if not self._second_third_resonance_exists and not fro:
            self._logger.warning("Second/Third resonance not computed =>"
                                 " Use only first resonance.")
            first_resonance_only = True
        # get qpts
        if first_resonance_only:
            qpts = []
            for interaction in interactions:
                qpts += self._get_all_qpts_from_first(interaction, **kwargs)
        else:
            qpts = []
            # merge all interactions in one plot. Because its a 2D histogram,
            # we couldn't see anything if we distinguish interactions
            for interaction in interactions:
                for branch in branches:
                    q, e = self._get_all_qpts_not_from_first(branch,
                                                             interaction,
                                                             **kwargs)
                    qpts += q.tolist()
        qpts = np.array(qpts)

        laser = self.checker.eL + self.checker.scissor_shift
        broad = self.checker.alpha
        mask = kwargs.get("mask", "none")
        title = (r"$\epsilon_L$=%.2f, broad=%s, mask=%s, path=%s,"
                 "branch=%s" % (laser, broad, mask, figname, str(branches)))
        # show / save figure
        if save is not None:
            if filename is None:
                filename = "qpt_2Dhist"
                filename += ("_laser%.2f_broad%s_interaction%s_mask%s" %
                             (laser, broad, figname, mask))
                if first_resonance_only:
                    filename += "_first_res"
                for branch in branches:
                    filename += "b" + str(branch)
                filename += ".png"
        # compute 2D histogram
        self._plot_2D_hist(qpts, bins=bins, title=title, show=show, save=save,
                           filename=filename, **kwargs)

    def _plot_2D_hist(self, vectors_list, bins=None, title=None,
                      show=True, save=None, filename=None, phonon_grid=True,
                      **kwargs):
        if phonon_grid:
            allvecs = self.checker.phonons.data["qpts"]
        else:
            allvecs = self.checker.electrons.data["kpts"]
        vecx = vectors_list[:, 0]
        vecy = vectors_list[:, 1]
        if bins is None:
            bins = int(np.sqrt(len(allvecs))) + 1
        delta = abs((allvecs[1, 1] - allvecs[0, 1]) / 2)
        # to shift qpts at the middle of bins
        xedges = np.linspace(allvecs[0, 0] - delta, allvecs[-1, 0]
                             + delta, bins)
        yedges = xedges
        H, xedges, yedges = np.histogram2d(vecx, vecy, (xedges, yedges))
        X, Y = np.meshgrid(xedges, yedges, indexing="ij")

        # plot histogram
        fig = plt.figure()
        ax = fig.add_subplot(111)
        im = ax.pcolormesh(X, Y, H, vmin=0, vmax=np.max(H))
        if phonon_grid:
            ax.set_xlabel(r"$q_x$")
            ax.set_ylabel(r"$q_y$")
        else:
            ax.set_xlabel(r"$k_x$")
            ax.set_ylabel(r"$k_y$")
        axes = kwargs.pop("axes", False)
        if axes:
            ax.axhline(0.0, color="w", linewidth=0.5)
            ax.axvline(0.0, color="w", linewidth=0.5)
        ax.set_xlim([-0.5, 0.5])
        ax.set_ylim([-0.5, 0.5])
        fig.colorbar(im)
        if title is not None:
            ax.set_title(title)
        # show / save figure
        if show:
            plt.show()
        if save is not None:
            if filename is None:
                filename = "name_me_plz.png"
            totalpath = os.path.join(save, filename)
            fig.savefig(totalpath)
        # delete figure to free memory
        # plt.close(fig)

    def plot_all_energy_histogram(self, only_all=False, **kwargs):
        path = kwargs.pop("save", None)
        if path is not None:
            allpath = os.path.join(path, "energyhist")
            if not os.path.isdir(allpath):
                os.mkdir(allpath)
        if not only_all:
            for i, interaction in enumerate(self.checker.interactions):
                self.plot_energy_histogram(interaction, save=allpath, **kwargs)
                for branch in self.checker.phonon_branches:
                    subpath = None
                    if path is not None:
                        # create subdir for all specific phooon branches
                        subpath = os.path.join(path, "b%i" % branch)
                        if not os.path.isdir(subpath):
                            os.mkdir(subpath)
                        energypath = os.path.join(subpath, "energyhist")
                        if not os.path.isdir(energypath):
                            os.mkdir(energypath)
                    self.plot_energy_histogram(interaction, save=energypath,
                                               phonons_branches=branch,
                                               **kwargs)
                    if not i:
                        # do this only once per branch
                        self.plot_energy_histogram("all",
                                                   phonons_branches=branch,
                                                   save=energypath, **kwargs)
        # all interactions with all branches
        self.plot_energy_histogram("all", save=allpath, **kwargs)

    def plot_energy_histogram(self,
                              interactions,
                              merge=True,
                              convolution=True, sigma=1.8,
                              extend=1000,
                              show=False, save=None, legend=True,
                              first_resonance_only=False,
                              filename=None,
                              phonons_branches="all",
                              **kwargs):
        """Method that plots and/or saves an energy histogram of the resonant
        phonons. Can also make a gaussian
        convolution to mimic experimental setup.

        Parameters
        ----------
        interactions : tuple, list
                       Names of the interactions to consider.
        merge : bool, optional
                If True, another curve (or hist) is added which corresponds
                to the sum of all other histograms.
        mask : str, optional {None, "rectangle", "circle"}
               Mask to apply on phonons to ignore phonons near gamma.
               If None, no mask is applied.
        convolution : bool, optional
                      If True, the convolution will be done.
        sigma : float, optional
                If convolution is True, this will set the std for the gaussian
                kernel used for the convolution.
        extend : int, optional
                 Number of zeros to add to each side of the histogram (useful
                 with the gaussian convolution to make the graphs go all
                 down to zero).
        show : bool, optional
               If True, the plot will be shown.
        save : str, optional
               If not None, this specifies the path of the
               image to save. Use filename to specify the file name.
        legend : bool, optional
                 If True, the legend will be shown.
        filename : str, optional
                   If None, a default filename will be used. If not, this
                   specifies the file name of the saved image.
        first_resonance_only : bool, optional
                               If True, the phonons considered will only be the
                               ones between first resonant kpts only. Else, it
                               will take the second resonance.
        phonons_branches : tuple, optional
                           Specifies the phonon branches to plot. Default is
                           (6, 10) which corresponds to A1g and A2g branches.
        """
        fro = first_resonance_only
        i, b, f = self._check_interactions_and_branches(interactions,
                                                        phonons_branches, fro)
        interactions, phonons_branches, figname = i, b, f

        if not self._second_third_resonance_exists and not fro:
            self._logger.warning("Second/Third resonance not computed =>"
                                 " Use only first resonance.")
            first_resonance_only = True
        laser = self.checker.eL + self.checker.scissor_shift
        broad = self.checker.alpha
        fig = plt.figure()
        ax = fig.add_subplot(111)

        # get qpts and energies
        all_qpts = self.checker.phonons.data["qpts"]
        all_energies = self.checker.phonons.data["frequencies"]

        if first_resonance_only:
            qpts = []
            for interaction in interactions:
                qpts = self._get_all_qpts_from_first(interaction, **kwargs)
            ph_energies = []
            for ib, branch in enumerate(phonons_branches):
                index = find_map_sorted(qpts, all_qpts)
                try:
                    energies = all_energies[index, branch] * EV_TO_CM
                except IndexError:
                    msg = ("Grids are not commensurable. Cannot get energy "
                           "histogram from first resonances...")
                    self._logger.critical(msg)
                    return
                ph_energies += energies.tolist()
            _energies = {"first_res": np.array(ph_energies)}
        else:
            # distinguish each histogram
            _energies = OrderedDict()
            for interaction in interactions:
                alle = []
                for branch in phonons_branches:
                    q, e = self._get_all_qpts_not_from_first(branch,
                                                             interaction,
                                                             **kwargs)
                    alle += e.tolist()
                alle = np.array(alle)
                _energies[interaction] = alle * EV_TO_CM
        if len(interactions) > 1 and merge:
            # merge all histograms into one
            all_e = []
            for e in _energies.values():
                all_e += e.tolist()
            all_e = np.array(all_e)
            _energies["-".join(interactions)] = all_e
        for i, (interaction, energies) in enumerate(_energies.items()):
            # plot histogram
            # set bins such that 1 bin width = 0.01 cm^-1
            totrange = np.max(energies) - np.min(energies)  # in cm^-1 here
            N = int(totrange / 0.01)
            if N == 0 or N == 1:
                # for example when there is no resonances, N might be 0
                N = 5
            hist, bins = np.histogram(energies, bins=N)
            width = bins[1] - bins[0]
            centers = (bins[:-1] + bins[1:]) / 2

            hist, centers = self._extend_array(hist, centers, n=extend)
            if convolution:
                hist = gaussian_filter1d(hist, sigma / width, mode="constant")
                ax.plot(centers, hist, label=interaction, color=COLORS[i],
                        linewidth=2.0)
            else:
                ax.plot(centers, hist, label=interaction, color=COLORS[i],
                        linestyle="steps", linewidth=2.0)
            emin = np.min(bins) - 10
            emax = np.max(bins) + 10
            ax.set_xlim([emin, emax])
        # draw vertical lines for gamma energies
        wheregamma = find_map_sorted(np.array([[0.0, 0.0, 0.0], ]),
                                     all_qpts)[0]
        egamma = all_energies[wheregamma] * EV_TO_CM
        # get energies for each qpts
        for ib, branch in enumerate(phonons_branches):
            # if convolution, extend the array
            if branch == 6:
                name = r"$A_g^1$"
            elif branch == 10:
                name = r"$A_g^2$"
            else:
                name = "branche #" + str(branch)
            label = name + r", $E_{\Gamma}=%.1f cm^{-1}$" % egamma[branch]
            ax.axvline(egamma[branch], color=COLORS[ib], linestyle='--',
                       label=label)
        # common to all plots
        ax.set_ylabel("Count")
        ax.set_xlabel(r"Phonon energy ($cm^{-1}$)")
        mask = kwargs.get("mask", "none")
        ax.set_title(r"laser=%.2f eV, broad=%s eV, mask=%s, interaction=%s,"
                     " branches=%s" %
                     (laser, broad, mask, "-".join(interactions),
                      "-".join([str(b) for b in phonons_branches])))
        if legend:
            handles, labels = ax.get_legend_handles_labels()
            lgd = ax.legend(handles, labels, loc='upper center',
                            bbox_to_anchor=(1.25, 1.0))
        if show:
            plt.show()
        if save is not None:
            if filename is None:
                filename = "qpt_energyhist"
                filename += ("_laser%.2f_broad%s_mask%s_interaction%s" %
                             (laser, broad, mask, figname))
                if convolution:
                    filename += "_gaussfilter"
                if first_resonance_only:
                    filename += "_first_res"
                filename += ".png"
            totalpath = os.path.join(save, filename)
            fig.savefig(totalpath, bbox_extra_artists=(lgd, ),
                        bbox_inches="tight")
        # delete figure to free memory
        # plt.close(fig)

    def _extend_array(self, histogram, centers, n=20):
        # extend an array with zeros on each side
        newhist = np.zeros(len(histogram) + 2 * n)
        newcenters = np.zeros(len(centers) + 2 * n)
        newhist[n:-n] = histogram
        newcenters[n:-n] = centers
        deltaE = centers[1] - centers[0]
        firstE = centers[0]
        lastE = centers[-1]
        for i in range(n):
            newcenters[i] = firstE - (n - i) * deltaE
            newcenters[-(i + 1)] = lastE + (n - i) * deltaE
        return newhist, newcenters

    def _get_all_qpts_not_from_first(self, branch, interaction, **kwargs):
        all_qpts = []
        energies = []
        for sec_third in (self.checker.second_resonance,
                          self.checker.third_resonance):
            res = sec_third[interaction]
            where = np.where(res["branch"] == branch)[0]
            all_qpts += res["qpt"][where].tolist()
            energies += res["wqnu"][where].tolist()
        all_qpts = np.array(all_qpts)
        energies = np.array(energies)
        keep = np.logical_not(self._inside_mask(all_qpts, interaction,
                                                **kwargs))
        all_qpts = all_qpts[keep]
        energies = energies[keep]
        if not len(all_qpts):
            raise ValueError("No qpt found.")
        return all_qpts, energies

    def _get_all_qpts_from_first(self, *args, **kwargs):
        kpts = self.checker.first_resonance["kpt"]
        n = len(kpts)
        diff_indices = np.array(np.meshgrid(range(n), range(n))).T.reshape(-1,
                                                                           2)
        d1 = diff_indices[:, 0]
        d2 = diff_indices[:, 1]
        all_qpts = kpts[d1, :] - kpts[d2, :]
        all_qpts = all_qpts[np.logical_not(self._inside_mask(all_qpts, *args,
                                                             **kwargs))]
        return all_qpts

    def _inside_mask(self, qpt, interaction,
                     mask=None, maxnorm=0.01, maxx=0.15, maxy=0.035,
                     anti_mask=False):
        # tells if a qpt is inside a mask.
        # gamma = (qpt == np.array([0.0, 0.0, 0.0])).all(axis=0)
        if qpt.ndim == 1:
            qpt = np.array([qpt.tolist(), ])
        if mask is None:
            mask = "none"
        mask = mask.lower()
        if mask not in ALL_MASKS:
            raise ValueError("Not a valid mask: %s" % mask)
        if mask == "none":
            # no mask => all qpt are not inside it
            # if interaction[-1] == "2" and gamma:
            if interaction.endswith("2"):
                to_return = np.linalg.norm(qpt, axis=1) <= 0.01
                # for interaction starting with a defect,
                # mask gamma diffusion
                # because it makes no sense diffusing on a defect of q=0
            else:
                to_return = np.zeros(qpt.shape[0])
        elif mask == "circle":
            to_return = np.linalg.norm(qpt, axis=1) <= maxnorm
        elif mask == "rectangle":
                to_return = np.logical_and((np.abs(qpt) <= maxx)[:, 0],
                                           (np.abs(qpt) <= maxy)[:, 1])
        if not anti_mask:
            return to_return
        else:
            return np.logical_not(to_return)
