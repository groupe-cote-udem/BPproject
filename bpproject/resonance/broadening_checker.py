import numpy as np
import logging
from .bandstructures import PhononsBandstructure, ElectronsBandstructure


class BroadeningChecker:
    def __init__(self,
                 phonon_path,
                 phonon_qpts,
                 electron_path,
                 electron_kpts,
                 lim_x=None,
                 lim_y=None,
                 maxband=None,
                 minband=0,
                 loglevel=logging.INFO):
        """Object that parses the eigenvalues to check the difference
        in energy for two adjacent kpt in the BZ.

        Parameters
        ----------
        phonon_path : str
                      Path to the phonons eigenvalues. Put this to None to not
                      compute the phonons broadening.
        phonon_qpts : str
                      Path to the corresponding list of qpts. Put this to None
                      to not compute the phonons broadening.
        electron_path : str
                        Path to the electrons eigenvalues. Put this to None to
                        not compute electrons broadening.
        electron_kpts : str
                        Path to the corresponding list of kpts. Put this to
                        None to not compute electrons broadening.
        lim_x : float, optional
                Limit (in absolute value) in the x direction for the BZ.
                (To narrow down the BZ).
        lim_y : float, optional
                Same as lim_x but for the y direction.
        maxband : int, optional
                  Gives the index of the maximal electronic band to consider.
                  If None, all bands are considered.
        minband : int, optional
                  Gives the index of the minimal electronic band to consider.
        loglevel : int, optional
                   Level for the logging object.
        """
        logging.basicConfig()
        self._logger = logging.getLogger("Broadening")
        self._logger.setLevel(loglevel)

        do_phonons = False
        do_electrons = False
        if phonon_path is not None and phonon_qpts is not None:
            do_phonons = True
        if electron_path is not None and electron_kpts is not None:
            do_electrons = True

        if not do_phonons and not do_electrons:
            raise ValueError("Specify something to check broadening plz.")
        if do_phonons:
            # acquire Data
            self._logger.info("Getting Phonon data...")
            self.phonons = PhononsBandstructure(phonon_qpts,
                                                phonon_path,
                                                flatten=False)
            self._logger.info(".. done.")
            qxlist = self.phonons.data["qx"]
            qylist = self.phonons.data["qy"]
            qzlist = self.phonons.data["qz"]
            phenergies = self.phonons.data["frequencies"]

        if do_electrons:
            self._logger.info("Getting electron data")
            self.electrons = ElectronsBandstructure(electron_path,
                                                    electron_kpts,
                                                    flatten=False)
            self._logger.info(".. done.")

            kxlist = self.electrons.data["kx"]
            kylist = self.electrons.data["ky"]
            kzlist = self.electrons.data["kz"]
            elenergies = self.electrons.data["energies"]
            if maxband is None:
                maxband = elenergies.shape[-1]
            # focus on specific bands
            elenergies = elenergies[:, :, :, minband:maxband]

        if lim_x is not None:
            # partial BZ
            self._logger.info("Partial BZ in the x direction.")
            if do_electrons:
                where_el_x = np.abs(kxlist) <= lim_x
                kxlist = kxlist[where_el_x]
                elenergies = elenergies[where_el_x]
            if do_phonons:
                where_ph_x = np.abs(qxlist) <= lim_x
                qxlist = qxlist[where_ph_x]
                phenergies = phenergies[where_ph_x]
        if lim_y is not None:
            # partial BZ
            self._logger.info("Partial BZ in the y direction.")
            if do_electrons:
                where_el_y = np.abs(kylist) <= lim_y
                kylist = kylist[where_el_y]
                elenergies = elenergies[:, where_el_y]
            if do_phonons:
                where_ph_y = np.abs(qylist) <= lim_y
                qylist = qylist[where_ph_y]
                phenergies = phenergies[:, where_ph_y]

        # compute broadening over full BZ
        self.electrons_broadening = None
        self.phonons_broadening = None
        if do_electrons:
            self._logger.info("Computing electrons broadening...")
            self.electrons_broadening = self._get_broadening(kxlist, kylist,
                                                             kzlist,
                                                             elenergies)
        if do_phonons:
            self._logger.info("Computing phonons broadening...")
            self.phonons_broadening = self._get_broadening(qxlist, qylist,
                                                           qzlist,
                                                           phenergies)

        self.print_results()

    def print_results(self):
        print("== Broadening Checker Results ==")
        el = self.electrons_broadening
        ph = self.phonons_broadening
        if el is not None:
            elmax = np.max(el["max"])
            wheremax = np.where(el["max"] == elmax)
            elmaxvec = el["vectors"][wheremax[:-1]]
            eldir = wheremax[-1][0]
            print("For Electrons:")
            print("-- max difference = %f at %s direction %i" %
                  (elmax, elmaxvec, eldir))
            print("-- mean difference = %f" % np.mean(el["mean"]))
            print("-- mean of the std of the difference = %f\n" %
                  np.mean(el["std"]))
        if ph is not None:
            phmax = np.max(ph["max"])
            wheremax = np.where(ph["max"] == phmax)
            phmaxvec = ph["vectors"][wheremax[:-1]]
            phdir = wheremax[-1][0]
            print("For Phonons:")
            print("-- max difference = %f at %s direction %i" %
                  (phmax, phmaxvec, phdir))
            print("-- mean difference = %f" % np.mean(ph["mean"]))
            print("-- mean of the std of the difference = %f\n" %
                  np.mean(ph["std"]))
        print("Directions: 0=left, 1=right, 2=up, 3=down, 4=upleft, 5=upright"
              ", 6=downleft, 7=downright")

    def _get_broadening(self, kxlist, kylist, kzlist, energies):
        nkx = len(kxlist)
        nky = len(kylist)
        #  nkz = len(kzlist)  # for now only 2D materials
        size = (nkx - 2, nky - 2, 8)  # 8 is for the 8 possible directions (2D)

        maximums = np.zeros(size)
        averages = np.zeros(size)
        std_dev = np.zeros(size)
        vectors = np.zeros((nkx - 2, nky - 2, 3))
        for ikx, kx in enumerate(kxlist[1:-1]):
            for iky, ky in enumerate(kylist[1:-1]):
                # +1 everywhere because we exclude
                # 0 everywhere in z because we check only for 2D for now
                here = energies[ikx + 1, iky + 1, 0]
                left = energies[ikx, iky + 1, 0]
                right = energies[ikx + 2, iky + 1, 0]
                up = energies[ikx + 1, iky + 2, 0]
                down = energies[ikx + 1, iky, 0]
                upleft = energies[ikx, iky + 2, 0]
                upright = energies[ikx + 2, iky + 2, 0]
                downleft = energies[ikx, iky, 0]
                downright = energies[ikx + 2, iky, 0]

                directions = (left, right, up, down, upleft, upright, downleft,
                              downright)
                for idir, direction in enumerate(directions):
                    diff = np.abs(np.subtract(here, direction))
                    maximums[ikx, iky, idir] = np.max(diff)
                    averages[ikx, iky, idir] = np.mean(diff)
                    std_dev[ikx, iky, idir] = np.std(diff)
                vectors[ikx, iky] = np.array([kx, ky, 0.0])
        return {"max": maximums, "mean": averages, "std": std_dev,
                "vectors": vectors}
