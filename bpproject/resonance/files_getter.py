import os


DATAPATH = os.path.join(os.path.dirname(__file__), "../../Data")


class FilesGetter:
    def __init__(self, kgridsize, qgridsize, nlayer, only=None):
        """Gets the files for data according to the size given.

        kgridsize : int
                    The size of the kpt grid (e.g. 100 for the 100 x 100 grid
        qgridsize : int
                    Same as above except for que qpt grid size.
        nlayer : int, {1 or 2}
                 The number of phosphorene layer. For now, only 1 or 2.
        """
        if nlayer not in (1, 2):
            raise ValueError("Only monolayer or bilayer phosphorene available")
        if nlayer == 1:
            datapath = os.path.join(DATAPATH, "monolayer")
        else:
            datapath = os.path.join(DATAPATH, "bilayer")
        self.epath = None
        self.ekpts = None
        self.ppath = None
        self.pqpts = None
        if only is None or only == "electrons":
            self.epath = os.path.join(datapath,
                                      "electrons/grid%i/eigenvalues%i" %
                                      (kgridsize, kgridsize))
            self.ekpts = os.path.join(datapath,
                                      "electrons/grid%i/kptgrid%i" %
                                      (kgridsize, kgridsize))
        if only is None or only == "phonons":
            self.ppath = os.path.join(datapath,
                                      "phonons/grid%i/phonons_bands_%i" %
                                      (qgridsize, qgridsize))
            self.pqpts = os.path.join(datapath,
                                      "phonons/grid%i/phonons_qpts_%i" %
                                      (qgridsize, qgridsize))
