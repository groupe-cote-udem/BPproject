from bpproject.resonance import (Histograms, ResonanceChecker,
                                 ResonanceMapper, ALL_MASKS)
import logging
import os
import traceback


class Runner:
    """Object that makes all calculations and saves all interesting graphs.
    """
    def __init__(self, savepath, *args, only_all=False,
                 show=True, bins=None, run=True,
                 **kwargs):
        loglevel = kwargs.get("loglevel", logging.INFO)
        logging.basicConfig()
        self._logger = logging.getLogger("Runner")
        self._logger.setLevel(loglevel)

        self._logger.info("Acquiring data...")
        self.checker = ResonanceChecker(*args, **kwargs)
        self.histograms = Histograms(self.checker, loglevel=loglevel)
        self.mapper = ResonanceMapper(self.checker, loglevel=loglevel)

        savepath = os.path.abspath(savepath)
        if run:
            try:
                self.plot_resonances(savepath, show, only_all=only_all)
                self.plot_histograms(savepath, show, bins=bins,
                                     only_all=only_all)
            except Exception as e:
                self._logger.error(str(traceback.print_tb(e.__traceback__)) +
                                   str(e))
                msg = ("Something happened :(, you might want to rerun the"
                       " Runner's methods to try again:")
                self._logger.critical(msg)
                self._logger.critical("=> runner.plot_resonances(%s, %s)" %
                                      (savepath, str(show)))
                self._logger.critical("=> runner.plot_histograms(%s, %s)" %
                                      (savepath, str(show)))

    def plot_resonances(self, savepath, show, **kwargs):
        savepath = os.path.abspath(savepath)
        if not os.path.isdir(savepath):
            self._logger.info("%s does not exists => creating it." % savepath)
            os.mkdir(savepath)
        # save resonances
        only_all = kwargs.pop("only_all", False)
        self._logger.info("Saving resonance plots with electron dispersions.")
        self._logger.info("Save path: %s" % savepath)
        self.mapper.el_dispersion_2D(save=savepath, show=show, **kwargs)
        self.mapper.el_dispersion(save=savepath, show=show, **kwargs)
        self.mapper.map_all_resonance(save=savepath, show=show,
                                      only_all=only_all, **kwargs)

    def plot_histograms(self, savepath, show, masks=ALL_MASKS,
                        **kwargs):
        savepath = os.path.abspath(savepath)
        if not os.path.isdir(savepath):
            self._logger.info("%s does not exists => creating it." % savepath)
            os.mkdir(savepath)
        self._logger.info("Saving histograms. Save to : %s" % savepath)
        # save histograms
        bins = kwargs.pop("bins", None)
        if isinstance(masks, str):
            masks = (masks, )
        for mask in masks:
            self._logger.info("Doing mask: %s" % mask)
            self.histograms.plot_all_energy_histogram(save=savepath,
                                                      show=show,
                                                      mask=mask, **kwargs)
            self.histograms.plot_all_2D_qpt_histogram(save=savepath,
                                                      show=show,
                                                      mask=mask, bins=bins,
                                                      **kwargs)
