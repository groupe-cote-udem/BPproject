#from .wfk import WFK
#from .resonance import ResonanceChecker

EV_TO_NM = 1239.84193
EV_TO_CM = 8065.73  # cm^-1 here
HA_TO_EV = 27.2114
ALL_INTERACTIONS = ("ee1", "ee2", "hh1", "hh2", "eh1", "eh2", "he1", "he2")
