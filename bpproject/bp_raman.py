import numpy as np
from intensities import Intensity


class BPSpectrum:
    def __init__(self, *args, **kwargs):
        """Computes the BP spectrum.
        """



class BPSpectrum:
    def __init__(self, w, w_L, dirac_FWHM, qpts, kpts, nbranches, Nd=0):
        """Object that computes the BP raman spectrum.

        w : array
            The array of frequencies to compute the raman intensity.
        w_L : float
              The incident light frequency
        dirac_FWHM : float
                     The FWHM of the lorentzian distribution
                     that replace the delta dirac distribution.
        qpts : array
               The grid of qpts for the phonons.
        kpts : array
               The grid of kpts for the electrons
        nbranches : int
                    The number of phonon branches considered.
                    Will probably be given
                    by the phonon dispersion from Julien... For now, assume it
                    is an input variable.
        Nd : float
             average number of defects in the unit cell
        """
        # the formulat for the spectrum contains a lot of parameters. A lot of
        # them are computed via other objects.
        # There are double phonon processes
        # and phonon-defect processes.
        self.phonon_defect_data = self.compute_phonon_defect(w_L, w, kpts,
                                                             qpts,
                                                             nbranches,
                                                             dirac_FWHM,
                                                             Nd)
        # for now we assume double phonon contributions are negligible
        self.double_phonon_data = self.compute_double_phonon(w_L, w, kpts,
                                                             qpts, nbranches,
                                                             dirac_FWHM)
        self.spectrum = self.compute_spectrum(self.phonon_defect_data,
                                              self.double_phonon_data)

    def compute_spectrum(self, pd_data, pp_data, qpts, nbranches):
        """Compute the spectrum from the data gathered. The output frequency
        dependency comes from the delta distribution.

        pd_data : data from phonon-defect processes
        pp_data : data from double-phonon processes
        """
        # each element of the dirac delta entries are arrays with a size
        # equal to the number of qpts
        Nq = len(qpts)
        # first sum for the pd process
        I1 = np.zeros(n_w)
        I2 = np.zeros(n_w)
        
        for iq, q in enumerate(qpts):
            for v in range(nbranches):
                I1 += (pd_data["Ipd"][iq, v] * 
                       pd_data["dirac_delta"][iq, v] *
                       (pd_data["bose_enstein"][iq, v] + 1))
                for u in range(nbranches):
                    I2 += (pp_data["Ipp"][iq, v, u] *
                           pp_data["dirac_delta"][iq, v, u] *
                           (pp_data["bose_einstein_v"][iq, v] + 1) *
                           (pp_data["bose_einstein_u"][iq, u] + 1))
        I1 /= Nq
        I2 /= Nq

        return I1 + I2

    def compute_double_phonon(self, w_L, w, kpts, qpts, nbranches,
                              dirac_FWHM):
        """Return a dictionary containing the information from phonon-defect
        processes. (For second sum in eq 2)
        
        w_L : float
              Frequency of incident light.
        w : array
            Array of frequencies to evaluate the raman spectrum.
        kpts : array
               The list of kpts.
        qpts : array
               The list of qpts.
        nbranches : int
                    The number of phonon branches.
        dirac_FWHM : float
                     The FWHM of the lorentz distribution that acts as a
                     dirac delta function.
        """
        data = {}
        I = Ipp(w_L, kpts, qpts, nbranches)
        data["Ipp"] = I.data["intensities"]
        d = np.zeros((len(w), len(qpts), nbranches, nbranches))
        wv = "phonon_frequencies_v"
        wu = "phonon_frequencies_u"
        for iq, q in enumerate(qpts):
            for v in range(nbranches):
                for u in range(nbranches):
                    d[:, iq, v, u] = self.dirac_delta(w_L - w -
                                                      # I assume here  that frequencies are
                                                      # already ordered in good way.
                                                      I.data[wv][iq, v],
                                                      I.data[wu][iq, u],
                                                      dirac_FWHM)
        data["dirac_delta"] = d
        data["bose_einstein_v"] = self.bose_einstein(I.data[wv])
        data["bose_einstein_u"] = self.bose_einstein(I.data[wu])
        return data

    def compute_phonon_defect(self, w_L, w, kpts, qpts, nbranches, dirac_FWHM, Nd):
        """Return a dictionary containing the information from phonon-defect
        processes. (For first sum in eq 2)

        w_L : float,
              Incident light frequency.
        w : array
            Frequency array to evaluate the raman spectrum.
        kpts : array
               The list of kpts.
        qpts : array
               The list of qpts.
        nbranches : int
                    The number of phonon branches.
        dirac_FWHM : float
                     FWHM for the lorentz distribution that acts as a dirac
                     delta.
        Nd : float
             Average number of defects in unit cell.
        """
        data = {}
        I = Ipd(w_L, kpts, qpts, nbranches, Nd)
        data["Ipd"] = I.data["intensities"]
        d = np.zeros((len(w), len(qpts), nbranches))
        for iq, q in enumerate(qpts):
            for v in range(nbranches):
                d[:, iq, v] = self.dirac_delta(w_L - w -
                                    # I assume here  that frequencies are
                                    # already ordered in good way.
                                    I.data["phonon_frequencies"][iq, v],
                                    dirac_FWHM)
        data["dirac_delta"] = d
        wq = "phonon_frequencies"
        data["bose_einstein"] = self.bose_einstein(I.data[wq])
        return data

    def delta(x, a):
        # distribution lorentzienne, a est la FWHM
        # x must be a numpy array!
        return 1 / (np.pi * a * (1 + (x / a) ** 2))

    def bose_einstein(w):
        # bose enstein distribution
        # assume hbar w >> kT => n(w) = 0 (to confirm later...)
        return np.zeros(len(w))
