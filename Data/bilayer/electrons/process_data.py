# from pybinit import Loader
from pybinit.parsers import OutputParser as Parser
# from bpproject.resonance.routines import find_map_sorted
import numpy as np
import os


def get_full_bz_points(unique_kx, unique_ky, unique_kz):
    # get the full zb points. for each kpt, check if -kpt is there.
    # if not, add it
    full_kx = []
    full_ky = []
    full_kz = []
    for klist, fulllist in zip((unique_kx, unique_ky, unique_kz),
                               (full_kx, full_ky, full_kz)):
        for k in klist:
            if -k not in klist and k != 0.5:
                # don't duplicate border
                fulllist.append(-k)
            fulllist.append(k)
    return np.sort(full_kx), np.sort(full_ky), np.sort(full_kz)


def get_reduced_kpts(kptslist):
    reduced = []
    for kpt in kptslist:
        if kpt[0] >= 0 and kpt[1] >= 0:
            reduced.append(kpt)
    return reduced


output_root = ("/Network/Servers/mance.pmc.umontreal.ca/Users/fgoudreault/"
               "Workspace/BPproject/Data/bilayer/electrons/grid1000/")
output = os.listdir(output_root)
output = [os.path.join(output_root, x) for x in output]
processed = ("/Network/Servers/mance.pmc.umontreal.ca/Users/fgoudreault/"
             "Workspace/BPproject/Data/bilayer/electrons/grid1000/"
             "eigenvalues1000")
kptgrid = ("/Network/Servers/mance.pmc.umontreal.ca/Users/fgoudreault/"
           "Workspace/BPproject/Data/bilayer/electrons/grid1000/kptgrid1000")
# output = ("/home/felix/Workspace/BPproject/Data/electrons/eigenvalues.out")
# processed = ("/home/felix/Workspace/BPproject/Data/electrons/eigenvalues")
# kptgrid = ("/home/felix/Workspace/BPproject/Data/electrons/kptgrid")
print("Parsing output files.")
eigens = []
for out in output:
    # open file and add trigger at beginning of file
    print("parsing %s" % os.path.basename(out))
    with open(out, "r+") as f:
        content = f.read()
        if "== DATASET 1" not in content:
            f.seek(0, 0)
            f.write("== DATASET 1\n" + content)
    parser = Parser(out, bypass_completeness=True, loglevel=100)
    eigens += parser.data["eigenvalues"]

nbands = len(eigens[0]["eigenvalues"])
kpt_to_write = []
eigen_to_write = []
print("Reorder elements")
for i, d in enumerate(eigens):
    kpt = d["coordinates"]
    if kpt[0] >= 0.0 and kpt[1] >= 0.0 and kpt[2] >= 0.0:
        kpt_to_write.append(kpt)
        eigen_to_write.append(d["eigenvalues"])
kpt_to_write = np.array(kpt_to_write)
eigen_to_write = np.array(eigen_to_write)
# we need to reorder the kpts
# find uniques
unique_kx = np.unique(kpt_to_write[:, 0])
unique_ky = np.unique(kpt_to_write[:, 1])
unique_kz = np.unique(kpt_to_write[:, 2])
# check if we cover the full BZ
print("Extending to full BZ")
print("Get full BZ coordinates")
(unique_kx2, unique_ky2, unique_kz2) = get_full_bz_points(unique_kx,
                                                          unique_ky,
                                                          unique_kz)
nx = len(unique_kx2)
ny = len(unique_ky2)
nz = len(unique_kz2)
new_kpts = np.zeros((nx * ny * nz, 3))
new_eigens = np.zeros((new_kpts.shape[0], nbands))
"""
new_kpts = np.vstack(np.meshgrid(unique_kx2, unique_ky2,
                                 unique_kz2)).reshape(3, -1).T
new_eigens = np.zeros((new_kpts.shape[0], nbands))
print(new_kpts.shape[0], kpt_to_write.shape[0])
if new_kpts.shape[0] <= kpt_to_write.shape[0]:
    raise ValueError("%s %s" % (str(new_kpts.shape),
                                str(kpt_to_write.shape)))
# iterate through unique vectors in order.
# and find index to reorder eigenvalues also
print("Get full BZ eigenvalues")
# copy what is already there
existing_index = find_map_sorted(kpt_to_write, new_kpts)
new_eigens[existing_index] = eigen_to_write
# find non_existing vectors and add them
all_rows_index = list(range(new_kpts.shape[0]))
non_existing = [x for x in all_rows_index if x not in existing_index]
l = len(non_existing)
print("There is %i non-existing vectors that need to be found." % l)
for n, i in enumerate(non_existing):
    flag_ok = False
    kpt = new_kpts[i]
    # check if -k is in full kpts
    kpt1 = np.multiply(kpt, np.array((-1, 1, 1)))
    kpt2 = np.multiply(kpt, np.array((1, -1, 1)))
    kpt3 = np.multiply(kpt, np.array((1, 1, -1)))
    kpt4 = np.multiply(kpt, np.array((-1, -1, 1)))
    kpt5 = np.multiply(kpt, np.array((-1, 1, -1)))
    kpt6 = np.multiply(kpt, np.array((1, -1, -1)))
    kpt7 = np.multiply(kpt, np.array((-1, -1, -1)))
    for trykpt in (kpt1, kpt2, kpt3, kpt4, kpt5, kpt6, kpt7):
        if any(np.equal(kpt_to_write, trykpt).all(1)):
            # yes it is. use this value for eigenvalues
            index = find_map_sorted(trykpt, kpt_to_write)
            new_eigens[i] = eigen_to_write[index]
            flag_ok = True
            break
    if not flag_ok:
        print("%s is not part of given kpts..." % str(kpt))
    print("%.1f / 100.0 done" % ((n + 1) / l))


reduced_rows = kpt_to_write.view([('', kpt_to_write.dtype)] *
                                  kpt_to_write.shape[1])
full_rows = full_kpts.view([('', full_kpts.dtype)] * full_kpts.shape[1])
non_existing_kpts = np.setdiff1d(reduced_rows,
                                 full_rows).view(kpt_to_write.dtype).
                                 reshape(-1, kpt_to_write.shape[1])
non_existing_index = find_map_sorted(non_existing_kpts)
"""
print("Get full BZ eigenvalues:")
for ikx, kx in enumerate(unique_kx2):
    print("%.1f / 100.0 done." % ((ikx + 1) / nx * 100))
    for iky, ky in enumerate(unique_ky2):
        for ikz, kz in enumerate(unique_kz2):
            vector = np.array([kx, ky, kz])
            kx2 = kx
            ky2 = ky
            kz2 = kz
            if kx not in unique_kx:
                kx2 = -kx
            if ky not in unique_ky:
                ky2 = -ky
            if kz not in unique_kz:
                kz2 = -kz
            vector_to_check = np.array([kx2, ky2, kz2])
            index = np.where((kpt_to_write ==
                              vector_to_check).all(axis=1))[0]
            where = ikz + iky * nz + ikx * ny * nz
            new_eigens[where, :] = eigen_to_write[index, :]
            new_kpts[where, :] = vector

with open(processed, "w") as fe:
    with open(kptgrid, "w") as fk:
        print("Writing files")
        print("nkpt = %i, neigenpt = %i" % (len(new_kpts), len(new_eigens)))
        for kpt, eigens in zip(new_kpts, new_eigens):
            fk.write(" ".join([str(x) for x in kpt]) + "\n")
            fe.write(" ".join([str(x) for x in eigens]) + "\n")
