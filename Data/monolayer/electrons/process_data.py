from pybinit import Loader
from pybinit.parsers import OutputParser as Parser
import numpy as np


def get_full_bz_points(unique_kx, unique_ky, unique_kz):
    # get the full zb points. for each kpt, check if -kpt is there.
    # if not, add it
    full_kx = []
    full_ky = []
    full_kz = []
    for klist, fulllist in zip((unique_kx, unique_ky, unique_kz),
                               (full_kx, full_ky, full_kz)):
        for k in klist:
            if -k not in klist and k != 0.5:
                # don't duplicate border
                fulllist.append(-k)
            fulllist.append(k)
    return np.sort(full_kx), np.sort(full_ky), np.sort(full_kz)


output = ("/Network/Servers/mance.pmc.umontreal.ca/Users/fgoudreault/"
          "Workspace/BPproject/Data/electrons/grid400/eigenvalues400.out")
processed = ("/Network/Servers/mance.pmc.umontreal.ca/Users/fgoudreault/"
             "Workspace/BPproject/Data/electrons/grid400/eigenvalues400")
kptgrid = ("/Network/Servers/mance.pmc.umontreal.ca/Users/fgoudreault/"
           "Workspace/BPproject/Data/electrons/grid400/kptgrid400")
# output = ("/home/felix/Workspace/BPproject/Data/electrons/eigenvalues.out")
# processed = ("/home/felix/Workspace/BPproject/Data/electrons/eigenvalues")
# kptgrid = ("/home/felix/Workspace/BPproject/Data/electrons/kptgrid")
print("Parsing output file.")
parser = Parser(output, bypass_completeness=True)
# loader = Loader(parser)
# eigens = loader.eigenvalues
eigens = parser.data["eigenvalues"]

kpt_to_write = np.zeros((len(eigens), 3))
nbands = len(eigens[0]["eigenvalues"])
eigen_to_write = np.zeros((len(eigens), nbands))

print("Reorder elements")
with open(processed, "w") as fe:
    with open(kptgrid, "w") as fk:
        for i, kpt in enumerate(eigens):
            kpt_to_write[i, :] = kpt["coordinates"]
            eigen_to_write[i, :] = kpt["eigenvalues"]
        # we need to reorder the kpts
        # find uniques
        unique_kx = np.unique(kpt_to_write[:, 0])
        unique_ky = np.unique(kpt_to_write[:, 1])
        unique_kz = np.unique(kpt_to_write[:, 2])
        # check if we cover the full BZ
        print("Extending to full BZ")
        (unique_kx2, unique_ky2, unique_kz2) = get_full_bz_points(unique_kx,
                                                                  unique_ky,
                                                                  unique_kz)
        nx = len(unique_kx2)
        ny = len(unique_ky2)
        nz = len(unique_kz2)
        length = nx * ny * nz  # total array length
        new_kpts = np.zeros((length, 3))
        new_eigens = np.zeros((length, nbands))
        # iterate through unique vectors in order.
        # and find index to reorder eigenvalues also
        for ikx, kx in enumerate(unique_kx2):
            for iky, ky in enumerate(unique_ky2):
                for ikz, kz in enumerate(unique_kz2):
                    vector = np.array([kx, ky, kz])
                    kx2 = kx
                    ky2 = ky
                    kz2 = kz
                    if kx not in unique_kx:
                        kx2 = -kx
                    if ky not in unique_ky:
                        ky2 = -ky
                    if kz not in unique_kz:
                        kz2 = -kz
                    vector_to_check = np.array([kx2, ky2, kz2])
                    index = np.where((kpt_to_write ==
                                      vector_to_check).all(axis=1))[0]
                    where = ikz + iky * nz + ikx * ny * nz
                    new_eigens[where, :] = eigen_to_write[index, :]
                    new_kpts[where, :] = vector
        print("Writing files")
        print("nkpt = %i, neigenpt = %i" % (len(new_kpts), len(new_eigens)))
        for kpt, eigens in zip(new_kpts, new_eigens):
            fk.write(" ".join([str(x) for x in kpt]) + "\n")
            fe.write(" ".join([str(x) for x in eigens]) + "\n")
