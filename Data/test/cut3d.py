from pybinit.utils import CUT3D


begin_kpt = 1
# end_kpt = 72
end_kpt = 1

begin_band = 1
# end_band = 15
end_band = 1

for ikpt in range(begin_kpt, end_kpt + 1):
    for iband in range(begin_band, end_band + 1):
        if ikpt == 1 and iband == 1:
            # keep the log for the first one only
            CUT3D("BP_GS_WFK", "BP_GS", kpt=ikpt, band=iband, keep_log=True)
        else:
            CUT3D("BP_GS_WFK", "BP_GS", kpt=ikpt, band=iband)
