from pybinit import Loader
from pybinit.parsers import OutputParser as Parser
import numpy as np


def get_full_bz_points(unique_kx, unique_ky, unique_kz):
    # get the full zb points. for each kpt, check if -kpt is there.
    # if not, add it
    full_kx = []
    full_ky = []
    full_kz = []
    for klist, fulllist in zip((unique_kx, unique_ky, unique_kz),
                               (full_kx, full_ky, full_kz)):
        for k in klist:
            if -k not in klist and k != 0.5:
                fulllist.append(-k)
            fulllist.append(k)
    return np.sort(full_kx), np.sort(full_ky), np.sort(full_kz)


print("Parsing output file.")
# parser = Parser(output, bypass_completeness=True)
# loader = Loader(parser)
# eigens = loader.eigenvalues
# eigens = parser.data["eigenvalues"]

# kpt_to_write = np.zeros((len(eigens), 3))
# nbands = len(eigens[0]["eigenvalues"])
# eigen_to_write = np.zeros((len(eigens), nbands))

## PHONONS
OUTPUTS = ("/Network/Servers/mance.pmc.umontreal.ca/Users/fgoudreault/"
           "Workspace/BPproject/Data/bilayer/phonons/grid500/qptgrid1-1",
           "/Network/Servers/mance.pmc.umontreal.ca/Users/fgoudreault/"
           "Workspace/BPproject/Data/bilayer/phonons/grid500/qptgrid1-2")
FREQS = ("/Network/Servers/mance.pmc.umontreal.ca/Users/fgoudreault/"
         "Workspace/BPproject/Data/bilayer/phonons/grid500/BP_Phonon3-1.out_B2EPS.freq",
         "/Network/Servers/mance.pmc.umontreal.ca/Users/fgoudreault/"
         "Workspace/BPproject/Data/bilayer/phonons/grid500/BP_Phonon3-2.out_B2EPS.freq")
allqpts = ("/Network/Servers/mance.pmc.umontreal.ca/Users/fgoudreault/"
           "Workspace/BPproject/Data/bilayer/phonons/grid500/phonons_qpts_500")
alleigens = ("/Network/Servers/mance.pmc.umontreal.ca/Users/fgoudreault/"
             "Workspace/BPproject/Data/bilayer/phonons/grid500/phonons_bands_500")
kpt_to_write = []
eigen_to_write = []
for freq, qpt in zip(FREQS, OUTPUTS):
    with open(freq) as f:
        lines = f.readlines()
        for line in lines:
            s = line.rstrip().split(" ")
            s = [float(x) for x in s if x]
            eigen_to_write.append(s)
    with open(qpt) as f:
        for line in f.readlines():
            s = line.rstrip().split(" ")
            s = [float(x) for x in s if x]
            kpt_to_write.append(s)

# remove unwanted elements
to_del = []
to_del_k = []
to_del_e = []
for i, qpt in enumerate(kpt_to_write):
    if np.round(qpt[0], 4) == -0.5000 or np.round(qpt[1], 4) == -0.5000:
        to_del.append(i)
#for j in to_del:
#    to_del_k.append(kpt_to_write[j])
#    to_del_e.append(eigen_to_write[j])
#print(to_del)
#for k, e in zip(to_del_k, to_del_e):
#    kpt_to_write.remove(k)
#    eigen_to_write.remove(e)
print(to_del)
kpt_to_write = np.array(kpt_to_write)
eigen_to_write = np.array(eigen_to_write)

new_kpts = kpt_to_write
new_eigens = eigen_to_write
print("Reorder elements")
# we need to reorder the kpts
# find uniques

unique_kx = np.unique(kpt_to_write[:, 0])
unique_ky = np.unique(kpt_to_write[:, 1])
unique_kz = np.unique(kpt_to_write[:, 2])

# check if we cover the full BZ
print("Extending to full BZ")
(unique_kx2, unique_ky2, unique_kz2) = get_full_bz_points(unique_kx,
                                                          unique_ky,
                                                          unique_kz)
nbands = eigen_to_write.shape[-1]
nx = len(unique_kx2)
ny = len(unique_ky2)
nz = len(unique_kz2)
length = nx * ny * nz  # total array length
new_kpts = np.zeros((length, 4))
new_eigens = np.zeros((length, nbands))
# iterate through unique vectors in order.
# and find index to reorder eigenvalues also
for ikx, kx in enumerate(unique_kx2):
    for iky, ky in enumerate(unique_ky2):
        for ikz, kz in enumerate(unique_kz2):
            where = ikz + iky * nz + ikx * ny * nz
            vector = np.array([kx, ky, kz, kpt_to_write[0, -1]])
            kx2 = kx
            ky2 = ky
            kz2 = kz
            if kx not in unique_kx:
                kx2 = -kx
            if ky not in unique_ky:
                ky2 = -ky
            if kz not in unique_kz:
                kz2 = -kz
            vector_to_check = np.array([kx2, ky2, kz2, kpt_to_write[0, -1]])
            index = np.where((kpt_to_write ==
                              vector_to_check).all(axis=1))[0]
            new_eigens[where, :] = eigen_to_write[index, :]
            new_kpts[where, :] = vector
print("Writing files")
print("nkpt = %i, neigenpt = %i" % (len(new_kpts), len(new_eigens)))
with open(allqpts, "w") as fk:
    with open(alleigens, "w") as fe:
        for i, (kpt, eigens) in enumerate(zip(new_kpts, new_eigens)):
            if i in to_del:
                # do not write unwanted qpt and its eigenvalues
                continue
            fk.write(" ".join([str(x) for x in kpt]) + "\n")
            fe.write(" ".join([str(x) for x in eigens]) + "\n")
